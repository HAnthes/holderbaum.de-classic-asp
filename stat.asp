<%
function MakeStat(LogPage, Comment, LookUp)

REM *********************************************************
REM ** Diese Funktion �bertr�gt Log-Daten in die Datenbank **
REM ** --------------------------------------------------- **
REM ** Version 3.0 - 00/10/09 - Stefan Mayer               **
REM *********************************************************

REM On Error Resume Next
Dim BC
Dim txtBro
Dim txtVer
Dim txtAgent
dim arrIp
Dim txtRefer
Dim txtHost
Dim conn
Dim RS
Dim x
REM Daten ermitteln: Browser & Version
Set BC = Server.CreateObject("MSWC.BrowserType")
txtBro = bc.browser
if txtBro = "" Then txtBro = "-"
txtVer = bc.Version
if txtVer = "" Then txtVer = "-"
Set BC = Nothing

REM Daten ermitteln: IP-Adresse und Host-Name [letzteres nur bei LookUp=TRUE]
arrIP = Split(Request.ServerVariables("REMOTE_ADDR"),".")
If LookUp Then
   Set DNS = Server.CreateObject("DNS.NSLookup") 
   txtHost = DNS.RevNSLookup(Request.ServerVariables("REMOTE_ADDR"))
   Set DNS = Nothing
Else
   txtHost = "-"
End If

REM Daten ermitteln: alles Andere   
txtAgent = Request.ServerVariables("HTTP_USER_AGENT")
if txtAgent = "" Then txtAgent = "-"
If Comment = "" Then
   txtRefer = Request.ServerVariables("HTTP_REFERER")
   if txtRefer = "" Then txtRefer = "-"
Else
   txtRefer = Comment
End If


Dim strProvider
strProvider = "PROVIDER=Microsoft.Jet.OLEDB.4.0;"
Dim strDatabase
strDatabase = "DATA SOURCE=" & Server.MapPath("/-db/log.mdb") & ";"
Dim strPassword
strPassword = "Jet OLEDB:Database Password=;"
Dim strComplete
strComplete = strProvider & strDatabase & strPassword
set Conn = Server.CreateObject("ADODB.Connection")
	    Conn.open strComplete
          

Set RS = CreateObject("ADODB.Recordset")
RS.Cursortype = 1
RS.LockType = 3
RS.Open "SELECT * FROM T_LOG WHERE ID=0", Conn

RS.AddNew
   RS("Seite") = LogPage 
   RS("IP1") = arrIp(0)
   RS("IP2") = arrIp(1)
   RS("IP3") = arrIp(2)
   RS("IP4") = arrIp(3)
   RS("Host") = txtHost
   RS("HTTP_USER_AGENT") = txtAgent 
   RS("HTTP_REFERER") = txtRefer
   RS("Browser") = txtBro
   RS("Version") = txtVer
RS.Update
RS.Close
Conn.Close

end function
%>