<%
REM Funktionsaufruf
REM	Call DoMail("von-mailkonto@meine-domain.de","empfaenger@zieldomain.de","Betrefftext","Bodytext")
	
	REM Beschreibung der Feldwerte: Call DoMail(cdoFrom,cdoTo,cdoSubject,cdoBody)
	
	
	Private Function DoMail(cdoFrom,cdoTo,cdoSubject,cdoBody)
		Session.LCID = 1031
		REM 	(p) 2007 by asppro.de, N. H�hnel: Fragen zu diesem Scipt bitte an support@asppro.de		

		Dim ObjSendMail
		Dim iConf
		Dim Flds
		Dim pickuppath
		    pickuppath = "D:\mailroot\Pickup"	'Diesen Pfad bitte so belassen, wird sich bis auf weiteres nicht �ndern.
		'Bitte bei Massensendungen beachten, nicht mehr als jeweils max. 99 Empf�nger in den BCC zu setzen.		

		Set ObjSendMail = CreateObject("CDO.Message")
		Set iConf = CreateObject("CDO.Configuration")
		Set Flds = iConf.Fields
		Flds("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
		Flds("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = pickupPath  
		Flds.Update
		Set ObjSendMail.Configuration = iConf
		
		ObjSendMail.To = cdoTo
		
		'ObjSendMail.CC = "..."
		'ObjSendMail.BCC = "..."

		ObjSendMail.From = cdoFrom

		ObjSendMail.Subject = cdoSubject

		ObjSendMail.TextBody = cdoBody

		'Optional HTML
		' ObjSendMail.HTMLBody = "<html>....</html>"
		ObjSendMail.Send
		Set ObjSendMail = Nothing
		
	End Function
%>