<%
'(C) 2004 Hans Anthes
Session.LCID = 1031
'HinterGrundSystem (Zeile 666 usw....
Dim bgbild
Dim bgxy 



'----------------------------------------------------------------------------------------------------------------------------------------------------------------
'Verlinkung nach layout_linker
'----------------------------------------------------------------------------------------------------------------------------------------------------------------
function linking(para,coType)

'SQL Parameter f�r die Abfrage und Listenaufbau
strSQL="SELECT * from layout_linker where type=" & coType & ";"
Dim LobjRS
Dim strText
Dim twopass
twopass=0
'Datenbankverbindung herstellen
Set LobjRS = Server.CreateObject("ADODB.Recordset")
LobjRS.Open strSQL,objConn, 1, 1
'Datemholen und Anzeigen
'Tabelle einleiten
linking=para
'wird ein zweiter durchlauf gebraucht ?  Suchen nach[twopass] k�rzel
if instr(linking,"{twopass}") then twopass=1

if not LobjRS.EOF then
           Do until LobjRS.EOF
		       strText=LobjRS("replaceLink")
 			   linking=replace(linking,LobjRS("searchWord"), strText) 
               LobjRS.MoveNext
           Loop
		   if twopass = 1 then
				LobjRS.MoveFirst
				Do until LobjRS.EOF	
					strText=LobjRS("replaceLink")
					linking=replace(linking,LobjRS("searchWord"), strText) 
					LobjRS.MoveNext
				Loop
			end if 
end if
    LobjRS.Close
end function
'----------------------------------------------------------------------------------------------------------------------------------------------------------------

'-----------------------------------------------------------------------------'
'Erzeugen der Configdaten f�r die Seite'
'-----------------------------------------------------------------------------'

    strSQL="SELECT * from layout_config;"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
    if not objRS.EOF then
       config_meta_daten=objRS("config_meta_daten")
       config_titel=objRS("config_titel")
       config_logo=objRS("config_logo")
	   config_menu_head=objRS("config_menu_head")
	   config_menu_foot=objRS("config_menu_foot")
      objRS.Close
    end if
'-----------------------------------------------------------------------------'
'Erzeugen der Configdaten f�r die Seite'
'-----------------------------------------------------------------------------'

'-----------------------------------------------------------------------------'
'Higlithing f�r Suche'
'-----------------------------------------------------------------------------'

'-----------------------------------------------------------------------------'
'IsValidEmail(email) �berpr�ft E-Mail auf Mailigkeit'
'-----------------------------------------------------------------------------'
'true gut false b�hhh'
'-----------------------------------------------------------------------------'
Function IsValidEmail(strEmail)
    Dim bIsValid
    bIsValid = True
    If Len(strEmail) < 5 Then
        bIsValid = False
    Else
        If Instr(1, strEmail, " ") <> 0 Then
	   bIsValid = False
        Else
            If InStr(1, strEmail, "@", 1) < 2 Then
                bIsValid = False
            Else
                If InStrRev(strEmail, ".") < InStr(1, strEmail, "@", 1) + 2 Then
                    bIsValid = False
                End If
            End If
        End If
    End If
    IsValidEmail = bIsValid
End Function
'-----------------------------------------------------------------------------'
'IsValidEmail(email) �berpr�ft E-Mail auf Mailigkeit'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
'Erzeugt Navigationsleiste links'
'DB layout_navigation und layout_navigation_head'
'-----------------------------------------------------------------------------'
sub navigation

    Dim searchlink
	Dim CX
	Dim Puffer
    strSQL="SELECT layout_navigation.*, layout_navigation_head.*, layout_navigation_head.seite FROM layout_navigation_head INNER JOIN layout_navigation ON layout_navigation_head.id = layout_navigation.head ORDER BY layout_navigation_head.rang, layout_navigation.ordnung;"

    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
  	if not objRS.EOF then
	  
	    menuKopf=""
	    start=0
		CX = 0 
		    Do until objRS.EOF
				CX = CX +1
				if menuKopf<>objRS("name") then
		        	if start=1 then Puffer = Puffer & "</menu>"&vbCRLF&"</div>"&vbCRLF
						menuKopf=objRS("name")
		          	    start=1
		               	Puffer = Puffer & "<div class=""menuhead"" id=""" & objRS("id") &"""><h2>"&objRS("name")&"</h2>"&vbCRLF
		               	Puffer = Puffer & "<menu>"&vbCRLF
		            end if
				   if objRS("titel") = "html" then
						Puffer = Puffer & "<li>" & objRS("link")& "</li>" &vbCRLF
					else
						Puffer = Puffer & "<li><a href="""&objRS("link")&""">"&objRS("titel")&"</a></li>"&vbCRLF
					end if 
				
		    objRS.MoveNext
		    Loop
			if CX > 0 then 
				response.write "<div id=""menu_l"">"
				response.write Puffer
				response.write "</menu></div>"&vbCRLF
			end if 
	end if
    objRS.Close
    response.write "</div>"&vbCRLF
end sub
'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'News_view'
'-----------------------------------------------------------------------------'
'Erzeugt die Newslist im Hauptbereich'
'DBs layout_news, layout_section'
'28.04.05  statischen defailt.asp link gegen Request.ServerVariables("SCRIPT_NAME") getauscht
'01.04.10 H1 ersatzlos entfallen, Div id="text wird zu new und kapselt einen news block
'01.04.10 Erweitert wird nicht mehr verwendet
'01.04.10 Zur�ckknopf ist weg
'01.04.10 H1 als News�berschrift definiert
'-----------------------------------------------------------------------------'
sub news_view()
%>
<div id="textbox">
<%

        Dim intNewsCounter, helper
        intNewsCounter=1

    strSQL="SELECT layout_news.n_index, layout_news.news_kopf, layout_news.News, layout_news.erweitert, layout_news.n_datum, layout_section.gruppe, layout_section.Bild, * FROM layout_news INNER JOIN layout_section ON layout_news.gruppe = layout_section.id ORDER BY layout_news.n_datum desc;"
	Set objRS = Server.CreateObject("ADODB.Recordset")
  	objRS.Open strSQL, objConn, 1, 1

  	if not objRS.EOF then
		Do until objRS.EOF or (intNewsCounter >max_page_item)
		if objRS("layout_section.gruppe")<>"Nicht Anzeigen" then
				helper = "<a class=""headup"" href=""" & Request.ServerVariables("SCRIPT_NAME") &"?action=news&detail=" & objRs("n_index") &"&Head=" & objRS("news_kopf") &""">[+]</a>"
				
				response.write "<div class=""news"">"& vbCRLF & "<a class=""na"" name=""" &objRS("n_index") & """></a>" & vbcrlf
				response.write "<div class=""inner"">" & linking(objRS("News"),1) &  "</div>" & vbCRLF
				

				'Quck and dirty Sticky l�sung!
				if objRS("layout_section.gruppe") ="Sticky" then
					response.write "<div class=""newsend""><a class=""headup"" href=""#logopoint"">[" & now() & "]</a></div>" & vbCRLF
				else
				response.write "<div class=""newsend""><a class=""headup"" href=""#logopoint"">[" & objRS("layout_section.gruppe") & " - " & objRS("n_datum")& "]</a>"& helper &"</div>" & vbCRLF
				end if 
				
				response.write "</div><!-- Letztes DIV im News Generator Loop -->"
				'�nderung f�r neues CSS - Nfoot nicht schreiben!
				'response.write "<div class=""Nfoot""></div>"
				intNewsCounter=intNewsCounter+1
			end if 	
			objRS.MoveNext
		Loop
	end if
	objRS.Close

 %>
</div>
<%
end sub
'-----------------------------------------------------------------------------'
'News_view'
'-----------------------------------------------------------------------------'

'-----------------------------------------------------------------------------'
'News_view_deatl : anzeigen eines Newsbeitrag mit Soczial Links'
'-----------------------------------------------------------------------------'
sub news_view_detail (strDetail)
dim helper
%>
<div id="textbox">
<%

	strSQL="SELECT * FROM layout_news WHERE n_index="& strDetail &";"

	Set objRS = Server.CreateObject("ADODB.Recordset")
  	objRS.Open strSQL, objConn, 1, &H0002

	if not objRS.EOF then
	   		    response.write "<div class=""Nhead""></div>"
				response.write "<div class=""news""><h1><a class=""na"" name=""" &objRS("n_index") & """>" & objRS("news_kopf") & "</a></h1>"&  vbCRLF
				response.write "<div class=""inner"">" & linking(objRS("News"),1) & "</div>"<!-- vor h3 -->" & vbCRLF
				response.write str_historyBackUrl & vbCRLF
				response.write "</div><!-- Letztes DIV im News Generator Loop -->"
				response.write "<div class=""Nfoot""></div>"
	end if
	objRS.Close

 %>
</div>
<%
end sub
'-----------------------------------------------------------------------------'
'News_view_deatl : anzeigen eines Newsbeitrag mit Soczial Links'
'-----------------------------------------------------------------------------'

'URL_Counter'

Sub URL_Counter( url )
    Dim help
    Dim ccobjRS
	if len(url)> 0 then
    strSQL="SELECT * from layout_url_counter where URL='" & url &"'"
	Set ccobjRS = Server.CreateObject("ADODB.Recordset")
  	ccobjRS.Open strSQL, objConn, 1, &H0002
  	if not ccobjRS.EOF then
	  	help=ccobjRS("counter")
		ccobjRS("counter")=help+1
	else
          ccobjRS.AddNew
          ccobjRS("counter")=1
		  ccobjRS("url") = url
        end if
        ccobjRS.Update
        ccobjRS.Close
	end if 
end sub

'-----------------------------------------------------------------------------'
'PageViewer (strDetail,url)
'contbox entfernt und nach Text umgestellt
'C_bezeichnung als �berschrift gesertzzt kein Dvi mehr im db
'-----------------------------------------------------------------------------'
Sub PageViewer (strDetail,url)
    Dim helper
	Dim Cache
    strSQL="SELECT * from layout_content where c_index=" & strDetail & ";"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    if not objRS.EOF then
       helper=objRS("c_counter")
       objRS("c_counter")=helper+1
	   objRS("c_datum")=date()
       objRS.Update
	   if objRS("c_html")= 2 then
          Cache = "<div id=""textbox""><div class=""Nhead""></div><div class=""news""><h1>" & objRS("c_bezeichnung") & "</h1><div class=""inner"">" &  linking(objRS("c_inhalt"),1)
		  
       else
          Cache = "<div id=""textbox""><div class=""Nhead""></div><div class=""news""><h1>" & objRS("c_bezeichnung") &"</h1><div class=""inner"">" & Replace(linking(objRS("c_inhalt"),1),vbCrLf,"<BR />" & vbCrLf)
       end if
	      if url<>"" then 
			Cache = replace(Cache,"<******LINK******>","<a href=""http://"&url&""">"&url&"</a>")
			call URL_Counter( url )
		  else
			Cache = replace(Cache,"<******LINK******>","") 
		  end if 
		  response.write Cache &"</div>"
		  response.write str_historyBackUrl & "</div><div class=""Nfoot""></div></div>"
    else
       response.write "<div id=""textbox""><div class=""Nhead""></div><div class=""news""><h1>Fehler bei der Anzeige</h1></div></div></div>"
    end if
    objRS.close
end Sub
'-----------------------------------------------------------------------------'
'PageViewer (strDetail)'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'EmployeeViewer(strDetail) strDetail = Abteilungsstring, ohne alle Anzeigen   '
'hier anpassen..... 2012
'-----------------------------------------------------------------------------'
Sub EmployeeViewer(strDetail)

	'Trennung zwischen Abteilung und Kontaktperson �ber strDetail, Abteilungsname Kontaktperson Nr.
	'Ohne strDetail - default Hallo@holderbaumn.de
	'Kn�pfe: E-Mail, vCard, vCardQr
	'E-Mail : Hover, EMailbild mit Infotext  Telefon und Fax
	'vCard  : Download vCard
	'vCardQr: QR-Code f�r die VCard Telefon und Fax
	
	
    Dim helper, strBild, strName, strAbt, strTelefon, strFax, strEMail, strButton, strInformation, errorCaptcha, startzeit
	
	if request("serial") = "" then 
	          startzeit = CDbl(now())
	else 
		startzeit = request("serial")
	end if 
	
	'Ermitteln der aufrufparameter
	if strDetail ="" then
		'nix, dann "Hallo"
		strBild    = ""
		strAbt     =""
		strName    ="Kontakt"
		strTelefon ="+49 631 316250" 
		strFax     ="+49 631 3162538"
		strEMail   ="hallo@holderbaum.de" 
		
		strInformation =            "Telefon : " & strTelefon & "<br /><br />" & vbCrLf
		strInformation = strInformation & "Fax     : " & strFax & "<br /><br />" & vbCrLf
		strInformation = strInformation & "Mail    : <img src=""/-img/bmps/personal/em/hallo.png""><br /><br />" & vbCrLf
		strButton="<img src=""/-img/bmps/personal/abt/hallo.png"" alt=""Holderbaum Kontakt""><br /><br />"
		strButton = strButton &  strInformation 
	else
		'Festlegen der Abteilung .... gruppierung �ber Abteilung!
		if len(strDetail) > 3 then 
			strSQL="SELECT layout_mitarbeiter.m_abteilung, layout_mitarbeiter.m_sammelmail, layout_mitarbeiter.m_fax, layout_mitarbeiter.m_sammeltel FROM layout_mitarbeiter GROUP BY layout_mitarbeiter.m_abteilung, layout_mitarbeiter.m_sammelmail, layout_mitarbeiter.m_fax, layout_mitarbeiter.m_sammeltel HAVING (((layout_mitarbeiter.m_abteilung)='"&strDetail&"'));"
			Set objRS = Server.CreateObject("ADODB.Recordset")
			objRS.Open strSQL, objConn,1 ,1
			if not objRS.EOF then
				Do until objRS.EOF
					strAbt     = objRS("m_abteilung") 
					strName    ="Kontakt - " 
					strTelefon =objRS("m_sammeltel")
					strFax     =objRS("m_fax")
					strEMail   =objRS("m_sammelmail")
					strBild = strAbt
					
					strInformation =            "Telefon : " & strTelefon & "<br /><br />" & vbCrLf
					strInformation = strInformation & "Fax     : " & strFax & "<br /><br />" & vbCrLf
					strInformation = strInformation & "Mail    : <img src=""/-img/bmps/personal/em/"  & strAbt & ".png""><br /><br />" & vbCrLf
					strButton="<img src=""/-img/bmps/personal/abt/" & strBild & ".png"" alt=""" & strName & " " & strAbt & """><br /><br />"
					strButton = strButton & strInformation
					objRS.MoveNext
				Loop
				objRS.Close
			end if
		end if
		
		'Kontaktperson gew�hlt
		if len(strDetail) < 3 and len(strDetail) > 0  then 
			strSQL="SELECT * FROM layout_mitarbeiter where m_index = " &strDetail
			Set objRS = Server.CreateObject("ADODB.Recordset")
			objRS.Open strSQL, objConn,1 ,1
			if not objRS.EOF then
				Do until objRS.EOF
				strName  = objRS("m_name") 
				strAbt = objRS("m_abteilung")
				strBild = objRS("m_bild")
				'strDetail = objRS("m_abteilung")
				strFax = objRS("m_fax")
				strTelefon = objRS("m_telefon")
				strEMail   =objRS("m_email")
				strButton="<img src=""/-img/bmps/personal/PicVC/" & strBild & ".png"" alt=""" & strName & " " & strAbt & """><br /><br />"
				strInformation =            "Telefon : " & strTelefon & "<br /><br />" & vbCrLf
				strInformation = strInformation & "Fax     : " & strFax & "<br /><br />" & vbCrLf
				strInformation = strInformation & "Mail    : <img src=""/-img/bmps/personal/em/"  & strBild & ".png""><br /><br />" & vbCrLf
				
				strButton = strButton &  strInformation 
				strInformation = "<a href=""/-img/bmps/personal/vcard/"& strBild &".vcf"">vCard (elekronische Visitenkarte)</a><br /><br /><img src=""/-img/bmps/personal/qr/" & strBild & ".png"">"
				strButton = strButton & strInformation 

				objRS.MoveNext
			Loop
			objRS.Close
		end if
	end if 
 
	
	end if 
'Abteilungs�bersicht erstellen'
    %>
    <div id="textbox">
		<div class="Nhead"></div>
			<div class="news"><h1><%=strName%>&nbsp;<%=strAbt%></h1>
			<div class="inner"> 
				<div style="float:left;">
			<% 
			'mit Parameter t=s dann haben wir eine Mail.
			'Anzeigen von Kontaktperson mit hinweise text
		
		
		  ' if server_response <> "" or newCaptcha then 
			'  if newCaptcha = False then
			'	 errorCaptcha=  "<div class=""law"">Captcha Fehlerhaft</div>"
			'  end if
			%>
					<form action="/default.asp?action=emp&detail=<%=strDetail%>&t=s" method="post">
						<fieldset><legend><a href="#kunde">Kunde*</a></legend>
						<br />
						Name:<br /><input type="text" id="name" name="name" value="<%=request("name")%>" />
						<br /><br />
						Telefon:<br /><input name="telefon" size="40" value="<%=request("telefon")%>" /><br /><br /> 
						Mail:<br /><input name="mail" size="40" value="<%=request("mail")%>" /><br /></fieldset>
						<span class="verification"><input name="verification" size="40"  /></span>
						<fieldset><legend><a href="#anliegen">Anliegen*</a></legend>
						<br /><textarea rows="6" name="Anliegen" cols="30"><%=request("Anliegen")%></textarea></fieldset>
						</fieldset>
						<input name="senden" type="submit" value="Senden" />
						<input name="serial" value="<%=startzeit%>" type="hidden"/>
					</form>
			<%		
			'else		
				if request("senden")="Senden" then	
				if request("Anliegen")= "" then
					response.write "<div class=""law"">Nachrichtenfeld ist leer.<br /><br />Zeit: " &now() & "<br /><br /></div>"
				else
					response.write "<div class=""GreenBox"">Ihre Nachricht wurde gesendet. <br /><br />Zeit: " &now() & "</div><br />"
					call SendCall( strEMail, request("Anliegen"), request("name"),request("telefon"),request("mail"), request("verification"), request("serial"))
			   end if 
			 end if  
			 'end if
			%>
			</div>
		
				<div style="text-align : center;"><%=strButton%></div>
				<div style="clear:left;"></div>
				<br />
<%
	if strDetail = "" then
		strSQL="SELECT layout_mitarbeiter.m_abteilung FROM layout_mitarbeiter GROUP BY layout_mitarbeiter.m_abteilung;"
		 Set objRS = Server.CreateObject("ADODB.Recordset")
			objRS.Open strSQL, objConn,1 ,1
			if not objRS.EOF then
				response.write "<fieldset><legend>Abteilungen:</legend><br />"
				Do until objRS.EOF
		
			%>
		
		<a href="/default.asp?action=emp&detail=<%=objRS("m_abteilung")%>"><img src="/-img/bmps/personal/abt/<%=objRS("m_abteilung")%>_1.png"><%=objRS("m_abteilung")%></a>
		<%
		    objRS.MoveNext
			Loop
			response.write "<br /></fieldset><br />"
			end if
			objRS.Close

	else
	if strDetail <>"" then helper="WHERE m_abteilung='" & strAbt & "';"

    strSQL="SELECT * FROM layout_mitarbeiter " & helper
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn,1 ,1
    if not objRS.EOF then
        Do until objRS.EOF
			response.write "<div class=""GreenBox"">" & vbCrLf
			response.write "<div style=""padding: 0 0 5px 0px""><img src=""/-img/bmps/personal/PicP/" & objRS("m_bild") & ".png""></div>" & vbCrLf
			response.write "<div style=""padding: 5px 5px 5px 5px;background:#f2f2f2;border-color:#d2d2d2;border-style:solid;border-width:1px 0pt;""><a href=""" & Request.ServerVariables("SCRIPT_NAME") & "?action=emp&detail="&objRS("m_index")&""">" & objRS("m_name") & " </a>&nbsp;&nbsp;(" & objRS("m_abteilung") &")</div>" & vbCrLf
			response.write "<div style=""margin: 2px 0 10px 0;padding: 5px 5px 5px 5px;background:#f4f4f4;border-color:#d2d2d2;border-style:solid;border-width:1px 0pt;"">" & objRS("m_beschreibung") & vbCrLf
			response.write "</div></div>"
            objRS.MoveNext
        Loop
    end if
    objRS.Close
end if
response.write linking("****Kontakt***Kunde",1)
response.write linking("****Kontakt***Anliegen",1)
response.write linking("****Kontakt***Captcha",1)
response.write linking("****Kontakt***Datenschutz",1)
%>	
</div>
<%
	
    response.write  str_historyBackUrl
    response.write "</div><div class=""Nfoot""></div></div>"
end Sub


Sub SendCall( strEMail, Anliegen,name,telefon,mail,spam, startZeit )
	dim helpmail, helptext
	'Nachrichten in Datenbank
   
   'Spamfilter scharf geschalet!
  if ((instr(Anliegen,"<a href") = 0) and (instr(Anliegen,"[url") = 0) and (instr(Anliegen,"[link") = 0) and spam ="" ) then
	strSQL="SELECT * FROM kd_nachrichten;"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    objRS.AddNew
    objRS("Empfang")=strEMail
	objRS("Anliegen") = Anliegen
	objRS("Name")=Name
	objRS("telefon")=telefon
	objRS("mail")=mail
	objRS("spam") = spam
	objRS("speed") =datediff("s",CDate(startzeit),now())
	objRs("datum")=now()
	objRS("koindex") = Friedmann (Anliegen)
	

	if IsValidEmail(mail) then 
		helpmail = mail
	else 
		helpmail = "hallo@holderbaum.de"
	end if
	helptext = "Name: " & name & vbCrLf & "Telefon:" & telefon & vbCrLf & "E-Mail:" & mail & vbCrLf & vbCrLf & vbCrLf &Anliegen
	Call DoMail(helpmail,strEMail,"Nachricht : Kontakt www.holderbaum.de" ,helptext)
   end if 
  
  objRS.UpDate
   objRS.Close
end Sub



'AnitSpam Friedmanindex

function Friedmann ( text )
	Dim i
  Dim zA(25)
  dim zsum
  dim temp
  dim tmp
  dim help
     for i=1 to Len(text)
	  	   tmp=Asc(Mid(text,i,1))
			if ( tmp >= asc("a") ) and ( tmp <= asc ("z")) then
			tmp = tmp - 97 'Nullmacche bzw Index
			zA(tmp) = zA(tmp) + 1
			zsum = zsum +1
		end if
next 

for i=0 to 25
	if zA(i) > 0 then help = zA(i)/zsum
	temp = temp + help * help
next

Friedmann = temp
end function


'.......................
'BG InfoSetzer of Hell


sub bgset

strSQL="SELECT * from layout_bg;"
Dim LobjRS
Dim tempDateA
Dim tempDateB
Dim tempDateC
Dim PictArray(10)
Dim XYArray(10)
Dim c, zw
c=1


Set LobjRS = Server.CreateObject("ADODB.Recordset")
LobjRS.Open strSQL,objConn, 1, 1
if not LobjRS.EOF then
           Do until LobjRS.EOF
			   tempDateA=DateValue(LobjRS("datumStart")) - DateValue("1.1." & Year(LobjRS("datumStart"))) 'Tage Start
			   tempDateB=DateValue(LobjRS("datumEnde")) - DateValue("1.1." & Year(LobjRS("datumEnde"))) 'Tage Ende
			   tempDateC = DateValue(now()) - DateValue("1.1." & Year(now())) 'Tage Jetzt
			   if (tempDateC > tempDateA) and (tempDateC < tempDateB) then
						PictArray(c) = LobjRS("bild")
						XYArray(c) = LobjRS("xy")
						c=c+1
						'response.write LobjRS("bild") & "A:" & tempDateA & LobjRS("datumStart") & " B:" & tempDateB & " C:" & tempDateC & "<br>"
				end if 
               LobjRS.MoveNext
           Loop
end if
    LobjRS.Close
	Randomize
	zw = int(((c-1)* rnd)+1)
	bgbild  = PictArray(zw)
	bgxy  = XYArray(zw)
end sub




sub HTMLHead (strTitel,strKeyWords,AddCSS)
dim bilderlink

Dim helpstrCar
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" >
<head>
<%=config_meta_daten%>
<meta name="keywords" content="<%=strTitel%>, <%=strKeyWords%>" />
<meta name="description" content="<%=left(strKeyWords,180)%>" />
<!-- SoMe OG-->
<meta property="og:title" content="<%=strTitel%>"/>
<meta property="og:description" content="<%=strKeyWords%>"/>
<%

if request("action") = "carview" and len(request("detail")) > 4 then
helpstrCar = "?action=carview&detail="&request("detail")
end if 
%>
<meta property="og:url" content="<%="http://" &request.servervariables("Server_Name") & request.servervariables("URL") & helpstrCar %>"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="de_DE"/>
<meta property="og:site_name" content="Autohaus am Stadtpark Holderbaum GmbH">
<%

bilderlink = "/-img/fzg/small/" & request("detail") & "_a.jpg"
		
if FileExists(bilderlink) <> 0  then
		   %>
<meta property="og:image" content="<%="http://" &request.servervariables("Server_Name") & bilderlink%>" />
<meta property="og:image:width" content="640" />
<meta property="og:image:height" content="480" />
		   <%
end if 


bilderlink = "/-img/fzg/small/" & request("detail") & "_f.jpg"
		
if FileExists(bilderlink) <> 0  then
		   %>
<meta property="og:image" content="<%="http://" &request.servervariables("Server_Name") & bilderlink%>" />
<meta property="og:image:width" content="662" />
<meta property="og:image:height" content="350" />
		   <%
end if 

%>

<title><%=left(strTitel,64)%></title>
<link rel="stylesheet" type="text/css" href="style/v1n.css" media="screen" title="new"/>
<%

'...bilder f�r sozme 600x315 640x640 800x800 1200x730  - uploadscript?!??!??!?
if AddCSS <>"" then 
	%>
	<link rel="stylesheet" type="text/css" href="style/<%=AddCSS%>" media="screen" title="new"/>
   <%
 end if 
%>
<link rel="alternate" type="application/rss+xml" title="RSS" href="rss.asp">
<script type="text/javascript" src="/lib/basis.js"></script>
<script type="text/javascript" src="/lib/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/lib/jquery.jcarousel.pack.js"></script>
<script type="text/javascript" src="/lib/jquery.fullscreenr.js"></script>

<script type="text/javascript" language="JavaScript">
<%call bgset %>
 jQuery.fn.fullscreenr({  <%=bgxy%>, bgID: '#bgimg' });
</script>
</head>
<body>
<img id="bgimg" src="/-img/img/<%=bgbild%>">
<%
bgset()
end Sub

sub HTMLEnd
%>
<script type="text/javascript" src="/lib/piqxr.js"></script>
</body>
</html>
<%
end sub

sub MakeKopf (strTitel,strKeyWords,AddCSS)
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	call HTMLHead (strTitel,strKeyWords,AddCSS)
%>


<div id="page">
<div id="logobox">
<%'LogoboxA entfernt Banner �berarbeiten tun
%>
<div id="logo"><a name="logopoint"></a><%=config_logo%></div>
</div>
<div id="headnav">
<div>
<%=config_menu_head%>
</div>
</div>

<%
'CALL navigation
end sub

sub MakeEnd
   %>
	
<div id="footnav">
<div>
<%
'call GetBrowser
response.write config_menu_foot
%>
</div>
</div>

	</div>
<%	
call HTMLEnd

end sub

'--------------------------------------------------------------------------------------------------------------------------------
'getContentHead(id) liefert die �berschrift von Contet
'--------------------------------------------------------------------------------------------------------------------------------
function getContentHead(strDetail)
	strSQL="SELECT * from layout_content where c_index=" & strDetail & ";"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    if not objRS.EOF then
		getContentHead=objRS("c_bezeichnung")		
	end if
    objRS.Close
end function

function getKeyWords(strDetail)
	strSQL="SELECT * from layout_content where c_index=" & strDetail & ";"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    if not objRS.EOF then
		getKeyWords=objRS("c_keywords")		
	end if
    objRS.Close
end function

function getCarHead(strDetail)
	strSQL="SELECT * from cars where SER_ID=" & strDetail &";"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    if not objRS.EOF then
		getCarHead=objRS("hersteller") &" "& objRS("modell") &", "& objRS("lack") &", "& objRS("leistung") &"kW, " & objRS("tacho") &"km"
		end if
    objRS.Close
end function


function getCarKeyWords(strDetail)
	strSQL="SELECT * from cars where SER_ID=" & strDetail &";"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1,&H0002
    if not objRS.EOF then
		getCarKeyWords=objRS("hersteller") &", "& objRS("modell") &", "& objRS("lack") &", "& MetaKomma2(left(objRS("be"),100),", ","", "")
		end if
    objRS.Close
end function


Function FileExistsFileExists(strFileName)
Dim FileName, fs
  set fs=Server.CreateObject("Scripting.FileSystemObject")
    FileName = Server.MapPath("./") & "\" & strFileName
	'FileName = Server.MapPath("strFileName")
    if fs.FileExists(Filename) then
        FileExists = True
    else
        FileExists = False
    end if
    set fs=nothing
End Function 

sub HTMLHeadPrint (strTitel,strKeyWords)
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" >
<%=config_meta_daten%>
<meta name="keywords" content="<%=strTitel%>, <%=strKeyWords%>" />
<meta name="Description" CONTENT="<%=strKeyWords%>" />
<head>
<title><%=strTitel%></title>
<link rel="stylesheet" type="text/css" href="style/print.css" media="screen" title="new"/>
<link rel="stylesheet" type="text/css" href="style/print.css" media="print" title="new"/>
</head>
<body>
<%
end Sub


%>

