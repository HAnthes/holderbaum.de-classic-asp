<%
'(C) 2004 Hans Anthes

Dim varCMS
varCMS="  - Ihr Volkswagen Service Partner in Kaiserslautern"
Dim varKeyWords
varKeyWords="VW Service, AU, HU, Diagnose, Unfallinstandsetzung, Volkswagen, Jahreswagen, Gebrauchtwagen, Golf, Polo, Touran, Passat, Diesel, VAS5052, Reimporte, EU-Neuwagen,mp3"
'Versiondaten, manuell :(
Dim V1_version
'V1_version="<img src=""/style/v1/sv1.jpg"" />" &"<br /><a href=""http://www.service2004.de/black/default.asp?action=con&detail=71"">V1 4.1 (27.10.2006)</a><a href=""http://www.service2004.de/black/default.asp?action=con&detail=71"">FZ 6.5 (17.11.06)</a>" 
'V1_version="<a href=""default.asp?action=con&detail=24""><img src=""/style/v1/impressum.jpg"" alt=""Link zum Impressum"">" & "<a href=""default.asp?action=carbrand""><img src=""/style/v1/cars.jpg"" alt=""Link zur Fahrzeug�bersicht"">" & "<a href=""default.asp?action=gb""><img src=""/style/v1/guestbook.jpg"" alt=""Link zum G�stebuch"">" & "<a href=""default.asp?action=emp""><img src=""/style/v1/kontakt.jpg"" alt=""Link zur Kontaktseite"">" & "<a href=""http://feedvalidator.org/check.cgi?url=http%3A//www.holderbaum.de/rss.asp""><img src=""valid-rss.png"" alt=""[Valid RSS]"" title=""Validate my RSS feed"" /></a>"

'Bilderbasis, wird von /config/bilder/pictures.asp verwendet
Dim Pic_Root
Pic_Root = "/-img/"

'Datenbankverbindung
Dim strProvider
strProvider = "PROVIDER=Microsoft.Jet.OLEDB.4.0;"
Dim strDatabase
strDatabase = "DATA SOURCE=" & Server.MapPath("/-db/autos.mdb") & ";"
Dim strPassword
strPassword = "Jet OLEDB:Database Password=;"
Dim strComplete
strComplete = strProvider & strDatabase & strPassword

'Z�r�cklink, wird an das Ende aller Seiten gesetzt
Dim str_historyBackUrl

str_historyBackUrl="<div class=""hback""><a href=""javascript:history.back();"">.::zur�ck::.</a></div>"

'Bilder und Links f�r das vor und zur�ck bei Seitenschaltungen
Dim vor_icon
'vor_icon = "<img src=""-img/layout/l.gif"">"
vor_icon="..:zur�ck:.."
Dim zu_icon
'zu_icon =  "<img src=""-img/layout/r.gif"">"
zu_icon="..:vor:.."
Dim spacer_text
spacer_text ="<img src=""-img/layout/sp.gif"" width=""10"">"

'Zumachknopf in Picturs.asp
Dim ausknopf
ausknopf = "-img/layout/aus.png"

'Faill�ngen f�r strAction und strDetail und strXDetail in Constants.asp hinterlegt
Dim failAction
Dim failDetail
Dim failXDetail
failAction=8
failDetail=25
failXDetail=6

'Alt!!!!!!
'EmployeeViewerTitel
Dim EmployeeViewerTitel
EmployeeViewerTitel="Kontakte:"

'Allgemeines, Anzahl der Reihen pro Seite bei Seitenschaltungen
Dim Max_Page_Item
Max_Page_Item=15
'Parameter f�r die Gallery v1_Gallery.asp

Dim strSQL
Dim objRS
Dim menuKopf, start
Dim config_meta_daten
Dim config_titel
Dim config_logo
Dim config_menu_head
Dim config_menu_foot
'Drucker Symbol mit Link
Dim Symbol_Print
Symbol_Print = "<a href=""javascript:self.print()""><img src=""-img/layout/printer.jpg"" border=""0"" alt=""Seite drucken""></a>"

'Rechtehinweise im GW-Detailanzeige
Dim str_gw_recht
str_gw_recht="<a name=""law"">Rechtliches:</a><br />Das Angebot ist unverbindlich und freibleibend. Irrtum und Zwischenverkauf vorbehalten.<br /><br />Angaben zu Emissionswerten:<br /> Die angegebenen Werte wurden nach den vorgeschriebenen Messverfahren (RL 80/1268/EWG in der gegenw�rtig geltenden Fassung) ermittelt. Die Angaben beziehen sich nicht auf ein einzelnes Fahrzeug und sind nicht Bestandteil des Angebotes, sondern dienen allein Vergleichszwecken zwischen den verschiedenen Fahrzeugtypen." 


'RSS-Feeder (nach RFC)

Dim RSS_Feed_Server_Name
Dim RSS_Feed_Title
Dim RSS_Feed_Description
Dim RSS_Feed_copyright
Dim RSS_Feed_TTL
Dim RSS_Feed_Image_Logo
Dim RSS_Feed_Image_Title
Dim RSS_Feed_Image_Link
Dim RSS_Feed_Author
Dim RSS_Feed_Disclaimer

function RCFDate(Var_Datum)
	
	Dim RSS_H_Hour
	Dim RSS_H_Min
	Dim RSS_H_Sec
	Dim RSS_H_Curr_Date
	Dim RSS_D_Day
	Dim RSS_D_NameofDay
	Dim RSS_D_NameofMonth
'Aktuelles Datum bereitstellen

RSS_H_Hour=Hour(Var_Datum)
RSS_H_Min=Minute(Var_Datum)
RSS_H_Sec=Second(Var_Datum)
RSS_D_Day=day(Var_Datum)

if RSS_H_Hour < 10 then RSS_H_Hour = "0" & RSS_H_Hour
if RSS_H_Min < 10 then RSS_H_Min = "0" & RSS_H_Min
if RSS_H_Sec < 10 then RSS_H_Sec = "0" & RSS_H_Sec
if RSS_D_Day < 10 then RSS_D_Day = "0" & RSS_D_Day


Select Case WeekDayName(WeekDay(Var_Datum),True)
		
        Case "Sa"
				RSS_D_NameofDay="Sat"
		Case "So"
				RSS_D_NameofDay="Sun"
		Case "Mo"
				RSS_D_NameofDay="Mon"				
		Case "Di"
				RSS_D_NameofDay="Tue"				
		Case "Mi"
				RSS_D_NameofDay="Wed"        				
		Case "Do"
				RSS_D_NameofDay="Thu"		
		Case "Fr"
				RSS_D_NameofDay="Fri"				
end select

select case Month(Var_Datum)
		case 1
			RSS_D_NameofMonth = "Jan"
		case 2
			RSS_D_NameofMonth = "Feb"
		case 3
			RSS_D_NameofMonth = "Mar"
		case 4
			RSS_D_NameofMonth = "Apr"
		case 5
			RSS_D_NameofMonth = "May"
		case 6
			RSS_D_NameofMonth = "Jun"
		case 7
			RSS_D_NameofMonth = "Jul"
		case 8
			RSS_D_NameofMonth = "Aug"			
		case 9
			RSS_D_NameofMonth = "Sep"
		case 10 
			RSS_D_NameofMonth = "Oct"
		case 11 
			RSS_D_NameofMonth = "Nov"
		case 12
			RSS_D_NameofMonth = "Dec"
		
			
end select

RCFDate=RSS_D_NameofDay & ", " & RSS_D_Day & " " & RSS_D_NameofMonth & " " & Year( Var_Datum) & " " & RSS_H_Hour & ":" & RSS_H_Min & ":" & RSS_H_Sec & " GMT"


end function



'Hostname festlegen, wird wegen der Internenverweise ben�tigt
RSS_Feed_Server_Name=Request.ServerVariables("Server_Name")

'Kopfdaten f�r den Feed

RSS_Feed_Title=Server.HTMLEncode("Autohaus am Stadtpark Holderbaum GmbH")
RSS_Feed_Description=Server.HTMLEncode("aktueller Fahrzeugbestand.")
RSS_Feed_copyright=Server.HTMLEncode("Inhalte: Autohaus am Stadtpark Holderbaum GmbH. Realisation: Hans Anthes")
RSS_Feed_TTL="240"
RSS_Feed_Image_Logo="http://" & RSS_Feed_Server_Name & "/webscreen.jpg"
RSS_Feed_Image_Title=Server.HTMLEncode("Logo von " & RSS_Feed_Server_Name)
RSS_Feed_Image_Link=Server.HTMLEncode("http://" & RSS_Feed_Server_Name)
RSS_Feed_Author=Server.HTMLEncode("webvkmail@holderbaum.de (Autohaus am Stadtpark, Abteilung Verkauf)")
RSS_Feed_Disclaimer="<p>" & str_gw_recht & "</p>"

'Mehrwerststeuer
Dim VarMwSt
VarMwSt=19

Dim str_v1_carsystem_ErrorBilder	

str_v1_carsystem_ErrorBilder =  "<div class=""law""><div style=""text-align:center;""><img src=""/style/v3/np.png"" alt=""keine Bilder da""></div><br />F�r dieses Fahrzeug ist leider noch kein Bild vorhanden.<br /><br />Wir werden die fehlenden Bilder schnellstm�glich nachpflegen.<br /><br />F�r weitere Fragen steht Ihnen unser <a href=""/default.asp?action=emp&detail=Verkauf"">Verkaufsteam</a> zur verf�gung.</div>"



'Einstellungen f�r die Mailfunktionen
Dim V1_MailHost 
V1_MailHost="localhost:2525"

'F�gt mit einem write den Wert in CarDetails ein - zb. F�r Finanzierungshinweise.
Dim TwoPassDetailCar
TwoPassDetailCar = "{twopass}*DetailCar"

%>