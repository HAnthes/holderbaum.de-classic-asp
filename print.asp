<%
'Druckseite
%>

<%Option Explicit%>

<!--#include file="-Vscripts/v1_parameter.asp" -->
<!--#include file="-Vscripts/v1_db_connect.asp" -->
<!--#include file="-Vscripts/v1_main.asp" -->
<!--#include file="-Vscripts/v1_carsystem.asp" -->
<!-- #Include file="stat.asp" --> 


<%

function EVO_Cat_LI(strCat,x)
Dim evoCatArray
dim i
Dim help, help1, help2
'help = 0
evoCatArray = Split(strCat,";")
if Ubound(evoCatArray) > 0  then 
	for i = 0 to UBound(evoCatArray) - 1 
	    help1= EVO_CAT_DB_Reader(evoCatArray(i),x)
		if help1<>"" then 
		help = help & "<li>"  & help1 & "</li>"
		end if
	next
end if 
EVO_Cat_LI = "<ul>" & help & "</ul>"
end function

function EVO_CAT_DB_Reader (evCatItem,x)
	Dim LobjRS
strSQL="SELECT evo_category.code, evo_category.description, evo_translate.subcat, evo_translate.Beschreibung, evo_translate.eva_id, evo_translate.mobile_Par FROM evo_category LEFT JOIN evo_translate ON StrComp(evo_category.code, evo_translate.eva_id,0)=0 WHERE ((strcomp(evo_category.code, '"& evCatItem &"',0)=0));"
	Set LobjRS = Server.CreateObject("ADODB.Recordset")
	LobjRS.Open strSQL,objConn, 1, 1
	if not LobjRS.EOF then
		Do until LobjRS.EOF
			   if Lobjrs("subcat") = x then  EVO_CAT_DB_Reader=LobjRS("description") 
		       LobjRS.MoveNext
         Loop
	end if 
	LobjRS.Close
end function

Function getBilderPr(fzID,alttext)
Dim a

Dim bild,help

help=""
for a = 0 to 3 
    bild = "/-img/fzg/small/" & fzID & "_" & a & ".jpg"
    if FileExists(bild) <> 0  then 
		help  =  help & "<img src=""" &"/-img/fzg/small/" & fzID & "_" & a & ".jpg" & """ name=""BigPic"" alt=""" & alttext &""" width=""130"" />" &vbCrLF
	end if 
next	
     bild = "/-img/fzg/qr/" & fzID & ".png"
	if FileExists(bild) <> 0  then 
		help  =  help & "<img src=""" & bild & """ alt=""QR-Code"" />" & vBCrLf
	end if
getBilderPr = help
end Function


Function PrDaten(Leistung,ez,tacho,treibstoff,Automatik,lack,tueren,tuv,au,hubraum,dpf,aufnr,sitze,allwheel,strHSN,strTSN)
	PrDaten = "<h1>Daten:</h1><ul><li>Erstzulassung: " & ez & "</li><li>Kilometerstand: " & tacho & " km</li><li>Leistung: " & leistung &" kW</li><li>Hubraum: " & hubraum & " cm�</li><li>Farbe: " & lack & "</li><li>"
    if automatik then
        PrDaten=PrDaten & "Automatik</li><li>"
    else
        PrDaten=PrDaten & "Schaltgetriebe</li><li>"
    end if
	Select case allwheel
			case 1
				PrDaten=PrDaten & "Allradantrieb</li><li>"
			case 2
				PrDaten=PrDaten & "Frontantrieb</li><li>"
			case 3
				PrDaten=PrDaten & "Heckradantrieb</li><li>"
	end select
	
	
    PrDaten=PrDaten & fueltest2(treibstoff,2)
	if dpf=1 then
	'	PrDaten=PrDaten&" - Dieselpartikelfilter"   
	end if
	PrDaten=PrDaten &"</li><li>" & tueren & " T�ren</li>"
	if sitze >0 then 
	    PrDaten=PrDaten & "<li>" & sitze & " Sitze</li>" 
	end if 
	
	PrDaten=PrDaten & "<li>HU/AU : " & tuv &"</li><li>HSN/TSN: " & strHSN & "/" & strTSN &"</li></ul>"
	
End Function




Dim HerstellerType, Daten, Umweltdaten, Ausstattung, Bilder, dpf, brutto, steuer, ustr, help, ausstattungCat, SubCat1, SubCat2, SubCat3

ustr = request("ac")
if (len (ustr) < 6) and (len (ustr) > 4 )  then 




'Datenbank �ffenen und Abfragen
	strSQL="SELECT cars.*, cars_info.*, cars_stat.*, cars.SER_ID FROM (cars LEFT JOIN cars_info ON cars.SER_ID = cars_info.seri) LEFT JOIN cars_stat ON cars.SER_ID = cars_stat.fzgnr WHERE (((cars.SER_ID)= "& ustr &"));"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL, objConn, 1, &H0002

	'Sind denn Daten da, Fahrzeug anzeigen
     if not objRS.EOF then
		'Hersteller...
		HerstellerType=objRS("hersteller") & " " & objRS("modell") 	 
		'Daten
		if instr(objRS("modell"),"DPF") <>0 then dpf=1
		Daten = PrDaten(objRS("LEISTUNG"),objRS("ez"),objRS("TACHO"),objRS("motor_id"),objRS("automatic"),objRS("lack"),objRS("tueren"),objRS("tuv"),objRS("au"),objRS("hubraum"),dpf,objRS("auf_nr"),objRS("CAR_SEATS"),objRS("allwheel"),objRS("hsn"),objRS("tsn"))
		
		
	'Umweltdaten
		
			if (cint(objRS("consump_mix"))<>0) or (cint(objRS("emis_badge"))<>0) or (cint(objRS("emis_Standard"))<>0) then
				Umweltdaten = "<h1>Umweltdaten:*</h1><ul>"
		    end if 
			if (cint(objRS("consump_mix"))<>0) and (cint(objRS("consump_road"))<>0) and (cint(objRS("consump_city"))<>0) and (cint(objRS("emiss_co2_mix"))<>0) then
				Umweltdaten = Umweltdaten & "<li>Verbrauch<ul><li>kombiniert: " & objRS("consump_mix") &"l/100km</li><li>au�erorts: " & objRS("consump_road") &"l/100km</li><li>innerorts: " & objRS("consump_city") &"l/100km</li></ul><li>C02 Emission (mix): " & objRS("emiss_co2_mix") & "mg</li>"
			end if 
			
			if objRS("emis_Standard")<>0 then 
				Umweltdaten = Umweltdaten & "<li>Abgasnorm: "
				Select case objRS("emis_standard")
					case 1
						Umweltdaten = Umweltdaten & "Euro 1"
					case 2
						Umweltdaten = Umweltdaten & "Euro 2"
					case 3
						Umweltdaten = Umweltdaten & "Euro 3"
					case 4
						Umweltdaten = Umweltdaten & "D 3"	
					case 5
						Umweltdaten = Umweltdaten & "Euro 4"					
					case 6
						Umweltdaten = Umweltdaten & "D 4"		
					case 7
						Umweltdaten = Umweltdaten & "Euro 5"		
					case 8
						Umweltdaten = Umweltdaten & "Euro 6"		
				End Select
				Umweltdaten = Umweltdaten &  "</li><li>"	  		
			end if 
			if objRS("emis_badge")<>0 then 
				Umweltdaten = Umweltdaten & "Umweltplakette: "
				Select case objRS("emis_badge")
				case 1
					Umweltdaten = Umweltdaten & "keine"
				case 2
					Umweltdaten = Umweltdaten & "gr�n"
				case 3
					Umweltdaten = Umweltdaten & "gelb"
				case 4
					Umweltdaten = Umweltdaten & "rot"	
				End Select
				Umweltdaten = Umweltdaten & "</li>"	  		
			end if 
			Umweltdaten = Umweltdaten &"</ul>" & vbCrLf
			
		Ausstattung = linking(linking(Server.HTMLEncode(metakomma(replace(objRS("be"),"!",""))),0),0)	
		
			'Preiserstellung
			if objRS("gesch_art") = "0" then
				steuer = "[ " & VarMwSt &"% MwSt ausweisbar]"
				brutto="<h1>" & FormatNumber(objRS("preis")*(VarMwSt/100+1),0,-1,0,-1) & " Euro</h1>" 
			else
				steuer = "[ Differenzbesteuert nach Par. 25a USTG ]"
				brutto="<h1>" & FormatNumber(objRS("preis"),0,-1,0,-1) & " Euro </h1>"
			end if
		
			end if 
			
	help = objRS("category")
	objRS.Close 
	SubCat1 = EVO_Cat_LI(help,1)
	SubCat2 = EVO_Cat_LI(help,2)
	SubCat3 = EVO_Cat_LI(help,3)
	ausstattungCat = "<ul><li>Aussen"&SubCat1&"</li><li>Innen"&SubCat2&"</li><li>Umwelt & Sicherheit"&SubCat3&"</li></ul>"
	
	
    Bilder=getBilderPr(ustr,HerstellerType)




'HTML Bis "Body"
Dim x
x = Makestat("Printer", "", false)
Call HTMLHeadPrint("Printseite : "&varCMS,"Druckausgabe v2.0")

'HTML Ger�st aufbauen
%>
<div id="Print">
<a href="javascript:window.print()">[diese Seite drucken]</a>
	<div id="Logo"><img src="/-img/layout/hoba.png" alt="Holderbaum-Logo" /></div>
		<div id="TextBereich">
			<div id="Kopf"><%=HerstellerType%></div>
			<div id="InfoBereich">
				<div id="T1">
					<%=Daten%>
					<%=Umweltdaten%>
					<div id="dis"><%=str_gw_recht &  "<br /><br />" & linking("**kraftstoff**",5)%></div>
					<div id="preis">
					<%=brutto%>
					<%=steuer%>
					</div>
				</div>
				<div id="T2">	
					<h1>Ausstattung:</h1>
					<%=ausstattungCat %>
				</div>
				<div id="T3">
					<h1>Beschreibung:</h1>
					<%=Ausstattung%>
				</div>
			</div>	
		</div>
	<div id="Bilder2">
					<%=Bilder%>
	</div>	
	<div id="Fuss">
	<h1>Autohaus am Stadtpark Holderbaum GmbH</h1>
	Pirmasenser Str. 57, D 67655 Kaiserslautern<br />
	Tel.: +49 (0) 631 / 316250  Fax: +49 (0) 631 / 3162550
	
	</div>
	
</div>	
<%
 
'HTML schluss 
Call HTMLEnd

end if 
%> 

