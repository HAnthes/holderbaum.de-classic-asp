<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0">
<%
Response.ContentType="text/xml"
%>
<!--#include file="-Vscripts/v1_parameter.asp" -->
<!--#include file="-Vscripts/v1_db_connect.asp" -->
<!--#include file="-Vscripts/v1_main.asp" -->
<!--#include file="-Vscripts/v1_carsystem.asp" -->
<channel>
	<title><%=RSS_Feed_Title%></title>
	<link><%=RSS_Feed_Image_Link%></link>
	<description><%=RSS_Feed_Description%></description>
	<language>de-de</language>
	<copyright><%=RSS_Feed_copyright%></copyright>
	<lastBuildDate><%= RCFDate(now())%></lastBuildDate>
	<ttl><%=RSS_Feed_TTL%></ttl>
	<image>
		<url><%=RSS_Feed_Image_Logo%></url>
		<title><%=RSS_Feed_Image_Title%></title>
		<link><%=RSS_Feed_Image_Link%></link>
	</image>
	<category>Fahrzeughandel</category>
	<generator>RSS.ASP V1.2(19.10.2006)</generator>
<%'RSS Autofeed 2.0'
'Benutzt  v1_db_connect.asp'
'wird in default.asp eingebunden <link rel="alternate" type="application/rss+xml" title="RSS" href="rss.asp">'
'15.02.07: Fehler bei der Treibstoffanzeige
'Alle Daten aus Cars

Dim ser_id
Dim title
Dim Preis
Dim Cat
Dim datum
Dim header
 
	strSQL="SELECT * from cars order by DATE_Created desc"
	objRS.Open strSQL, objConn
	Do While (Not objRS.EOF)
	if objRS("gesch_art") = "0" then
			brutto=objRS("preis")*(VarMwSt/100+1)
		else
			brutto=objRS("preis")
		end if	
	if instr(objRS("modell"),"DPF") <>0 then dpf=0
ser_id = objRS("ser_id")
title = Server.HTMLEncode(objRS("hersteller") & " " & objRS("modell"))&", Ez.:" & objRS("ez") & ", "& FormatNumber(brutto,0,-1,0,-1) & " Euro"
cat = Server.HTMLEncode(objRS("hersteller")&"/"&objRS("aufbau"))
datum = objRS("DATE_Created")
header = Server.HTMLEncode(objRS("hersteller") &" "& objRS("modell") &", "& objRS("lack") &", "& objRS("leistung") &"kW, " & objRS("tacho") &"km, "& MetaKomma2(objRS("be"),", ","", ""))%>
<item>
<title><%=title%></title>
<link>http://<%=RSS_Feed_Server_Name%>/default.asp?action=carview&amp;detail=<%=ser_id%></link>
<guid><%=ser_id%></guid>
<description><%=header%></description>
<author><%=RSS_Feed_Author%></author>
<category><%=cat %></category>
<pubDate><%=RCFDate(datum)%></pubDate>
</item>
<%
objRS.MoveNext
loop
objRS.Close
%>
</channel>
</rss>
<!--#include file="-Vscripts/v1_db_close.asp" --> 