<%
'(C) 2004 Hans Anthes
'08.12.04: switch.js einfe�gt zur Style ver�nderung'
'08.12.04: Ordner Style einf�hrt zum Speichern von Layouts und Switch.js
'15.12.04: Faill�ngen f�r strAction und strDetail und strXDetail in Constants.asp hinterlegt
'20.12.04: Entr�mpelt und Scriptnamen angepasst
'13.01.05: Aufr�umen und Doku
'04.04.05: Modelltrenung in Fahrzeug�bersicht, ein Parameter erg�ngzt.
'21.11.05: rss.asp eingbunden. 
'	    : rss.asp 1.0 stellt Auto RSS-Feed bereit
'24.11.05: Fehlerhaftes META-Tag aus dem Head Bereich entfernt.
'24.11.05: Styleswitch JScript Include entfernt
'05.09.06 Funktion MakeKopf erzeug den einleitenden HTML Code und setzt einen Titel
'21.02.07: K�pfe werden genauer erstellt. mit getContentHead und GetCarHead.
'21.02.07: Variabele varCMS wird als @ an den Kopf geh�ngt

%>

<%Option Explicit%>


<!--#include file="-Vscripts/inc_sha256.asp" -->
<!--#include file="-Vscripts/v1_parameter.asp" -->
<!--#include file="-Vscripts/v1_db_connect.asp" -->
<!--#include file="-Vscripts/v1_main.asp" -->
<!--#include file="-Vscripts/v1_carsystem.asp" -->
<!--#include file="-Vscripts/mail.asp" -->
<!-- #Include file="stat.asp" --> 

<% 
Dim x
'x = Makestat("Default", "", false)

    
	
	Dim strAction
    Dim strDetail
    Dim strKomentarAdd
    Dim strXDetail
	Dim sr
	Dim sf
	Dim url
	dim head
	dim HelpY
	dim HelpX
    strAction=request("action")
    strDetail=request("detail")
    strKomentarAdd=request("senden")
    strXDetail=request("xdetail")
	sr=request("sr")
	sf=request("sf")
	url=request("url")
	head=request("head")
    'Navigationsleiste aufbauen'
	'L�ngencheck der �bergebenen Parameter
    if len(head) > 50 then head = "Infos " 
	if instr(head,">") <> 0 then head = "Infos "
	if (len(strAction) > failAction) or (len(strDetail) >failDetail) or (len(strXDetail) > failXDetail) or (len(sr)>=2) or (len(sf)>=40)  then strAction = "fail"
	
    'Modul Wahl
    Select Case strAction
		'15.11.2012 - neue Funktion News als einzelseite z.b. verlinknen in FB Variable Head wird gefilterter
        Case "news"
			 Call MakeKopf(head & " - www.holderbaum.de", varKeyWords,"")
			 CALL navigation
             CALL news_view_detail (strDetail)

	        'Anzeigen der Statischen Seiten
        Case "con"
  		     Call MakeKopf(getContentHead(strDetail)&varCMS,getKeyWords(strDetail),"")
			 CALL navigation
             CALL PageViewer (strDetail,url)
	
    'Anzeigen der User�bersicht
        Case "emp"
			 Call MakeKopf("Kontake "&strDetail&varCMS,varKeyWords,"v1e.css")
			 CALL navigation
             CALL EmployeeViewer(strDetail)

        'Mailnachricht an einen User verfassen
        Case "empm"
			 Call MakeKopf("EMail an unsere Mitarbeiter"&varCMS,varKeyWords,"v1e.css")
			 CALL navigation
			 CALL EmployeeMail(strDetail,"Kontakt","?action=empm&detail="&strDetail,strKomentarAdd)

			
        'Carsystem: Anfrage ohne DB
        Case "carcall"
			 Call MakeKopf("Fahzeuganfrage"&varCMS,varKeyWords,"")
			 CALL navigation
             Call CarCall

		'Carsystem: Anzeige der Fahrzeuge einer Marke (strDetail Markenname)
        Case "carlist"
		  	 Call MakeKopf("Fahrzeugliste gefiltert nach Marke: "& strDetail & " / Modell: " & request("modelle")&varCMS,varKeyWords,"v1car.css")
			 'CALL Car_navigation (strDetail,request("aufbau"))
			 CALL navigation
             Call CarList(strDetail,request("modelle"),request("aufbau"),sr,sf)

		'Carsystem: Detailanzeige eines Fahrzeug (strDetail Car_ID)
        Case "carview"
  			 Call MakeKopf(getCarHead(strDetail)&varCMS,getCarKeyWords(strDetail),"v1detail.css")
			'Aufbereitung f�r Marken und Aufbau Trenneng
			'-----f�r Car_navigation - wieder entfertnt muss noch �berarbeitet werden.
			'Helpx=request("x")
			'if HelpX = "%" then 	HelpX = "*"
			'HelpY=request("y")
			'if HelpY = "%" then HelpY = "*"
			 'CALL Car_navigation (HelpX,HelpY)
			 CALL navigation
			 Call CarView(strDetail)

		'Carsystem: Anfrage f�r dieses Fahrzeug
        Case "carreq"
			 Call MakeKopf(config_titel&varCMS,varKeyWords,"v1detail.css")
			 'Aufbereitung f�r Marken und Aufbau Trenneng
			 '-----f�r Car_navigation - wieder entfertnt muss noch �berarbeitet werden.
			 'Helpx=request("x")
			'if HelpX = "%" then 	HelpX = "*"
			'HelpY=request("y")
			'if HelpY = "%" then HelpY = "*"
	     	'CALL Car_navigation (request("y"),request("x"))
			 CALL navigation
             Call CarReq(strDetail)
			 'ReCaptcha - Formularauswertung innerhalb vom VarView realisiert
			 Call CarView(strDetail)
		'12.04.2012 - Online Termine
		Case "kdterm"
			Call MakeKopf("KundendienstTerminanfrage"&varCMS,varKeyWords,"")
			CALL navigation

			Call KDTermin
	   'Im Fehlerfall nur Startseite mit News�bersichtzeigen.
       Case "fail"
	   	     Call MakeKopf(config_titel&varCMS,varKeyWords,"")
			 CALL navigation

             Call news_view
       Case Else
			 Call MakeKopf(config_titel&varCMS,varKeyWords,"")
			 CALL navigation

             Call news_view
end select
call makeEnd
%> 