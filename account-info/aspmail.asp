<%
Set Mailer = Server.CreateObject("SMTPsvg.Mailer")

    ' --- Da ASPMail standardm�ssig 7 Bit Ascii verwendet, werden
    ' --- Umlaute normalerweise nicht korrekt angezeigt. Um dies
    ' --- zu umgehen, wird CharSet auf 2 gestellt.
    Mailer.CharSet = 2

    ' --- Absender und Mailserverangaben zuweisen
    Mailer.Organization = "Ihre Firma"
    Mailer.FromName = "Ihr Firmenname"
    Mailer.FromAddress = "info@ihrefirma.de"
    Mailer.RemoteHost = "localhost:2525"

    ' --- Empf�nger initialisieren (muss bei mehrfachem Aufruf
    ' --- in Schleifen auf jeden Fall gemacht werden)
    Mailer.ClearAllRecipients

    ' --- Empf�nger hinzuf�gen
    Mailer.AddRecipient "Irgendein Empf�nger", "info@asp-solutions.de"

    Mailer.Subject = "Betreff"

    ' --- Den eigentlichen Mailtext initialisieren (muss bei mehrfachem
    ' --- Aufruf in Schleifen auf jeden Fall gemacht werden)
    Mailer.ClearBodyText
    Mailer.BodyText = "Text der Mail"

    ' --- Mit diesem Aufruf wird die Mail versendet. Der R�ckgabewert True
    ' --- besagt, die Mail wurde versendet. Bei False ist ein Fehler (kann mit
    ' --- Mailer.Response ausgelesen werden) aufgetreten.
    If Mailer.SendMail Then
        SendMail = True
        m_errortext = "Die eMail wurde erfolgreich versendet."
        Else
            SendMail = False
            m_errortext = Mailer.Response
    End if

    Response.Write m_errortext

Set Mailer = Nothing
%>
