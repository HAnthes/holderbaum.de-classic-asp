<%Option Explicit
'19.05.05 Feld Seite f�r Men�kopf eingef�hrt 


Sub menuNew(qryChange,qryDetail)
if qryChange="head" then 
     Call menuEditFormHead("","","","")
end if 
if qryChange="punkt" then 
      Call menuEditFormPunkt("","","","","")
end if 
end Sub


Sub menuKill(qryChange,qryDetail)
'ist qryDetail gegeben dann ein alter DS ist nicht dann eine Neuer'
'zuerst where'
Dim strWhere
if qryChange="head" then 
    strWhere=" where id=" & qryDetail & ";"
    strSQL="select * from layout_navigation_head" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end if

'Menupunkte'
if qryChange="punkt" then 
   
   strWhere=" where index=" & qryDetail & ";"
   strSQL="select * from layout_navigation" & strWhere
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, &H0002
   objRS.Delete
   objRS.Close
   
end if
end Sub


Sub addDBMenu(qryChange,qryDetail)        
'ist qryDetail gegeben dann ein alter DS ist nicht dann eine Neuer'
'zuerst where'
Dim strWhere

if qryChange="head" then 
    if qryDetail="" then 
      strWhere=";"
    else
      strWhere=" where id=" & qryDetail & ";"
    end if 
    strSQL="select * from layout_navigation_head" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("name")=request("titel")
    objRS("rang")=request("rang")
	objRS("seite")=request("seite")
    objRS.UpDate
    objRS.Close
end if

'Menupunkte'
if qryChange="punkt" then 
   if qryDetail="" then 
      strWhere=";"
    else
      strWhere=" where index=" & qryDetail & ";"
    end if 
   strSQL="select * from layout_navigation" & strWhere
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, &H0002
   if qryDetail="" then objRS.AddNew
   objRS("titel")=request("titel")
   objRS("link")=request("link")
   objRS("ordnung")=request("ordnung")
   objRS("head")=request("head")
   objRS.UpDate
   objRS.Close
   
end if
end Sub

'-----------------------------------------------------------------------------'
'menuEditFormHead'
'-----------------------------------------------------------------------------'
'Formular f�r die Daten�nderung '
'----------------------------------------------------------------------------'
Sub menuEditFormHead(strTitel,intRang,intId,intSeite)
%>
<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?change=head&detail=<%=intId%>" method="post">
<tr>
<td bgcolor="#BBCCFF">Titel:</td><td><input name="titel" size="40" value="<%=strTitel%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Rang:</td><td><input name="rang" size="40" value="<%=intRang%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Seite:</td><td><input name="seite" size="40" value="<%=intSeite%>"></td>
</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="2"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</tr>
</table>
<%
end Sub
'-----------------------------------------------------------------------------'
'menuEditFormHead'
'-----------------------------------------------------------------------------'

'-----------------------------------------------------------------------------'
'menuEditFormPunkt'
'-----------------------------------------------------------------------------'
'Formular f�r die Daten�nderung '
'----------------------------------------------------------------------------'
Sub menuEditFormPunkt(strTitel,strLink,intRang,intHead,intId)
%>
<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?change=punkt&detail=<%=intId%>" method="post">
<tr>
   <td bgcolor="#BBCCFF">Titel:</td><td><input name="titel" size="40" value="<%=strTitel%>"></td>
   <td bgcolor="#BBCCFF">Rang:</td><td><input name="ordnung" size="40" value="<%=intRang%>"></td>
</tr>
<tr>
   <td bgcolor="#BBCCFF">Link:</td><td><input name="link" size="40" value="<%=Server.HTMLEncode(strLink)%>"></td>
   <td bgcolor="#BBCCFF">Kopf Zuordung</td><td><select name="head" size="1">
   <%
   'Dropdown f�r K�pfe
    strSQL="select * from layout_navigation_head;"
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
    if not objRS.EOF then
        Do until objRS.EOF
           response.write "<option value=""" & objRS("id") & """"
           if (intHead)=(objRS("id")) then
              response.write " selected "
           end if 
           response.write ">" & objRS("name") & "</option>"
           objRS.MoveNext
         Loop
    end if
    objRS.Close     
   %> </select>
   </td>
</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="4"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</tr>
</table>
<%
end Sub
'-----------------------------------------------------------------------------'
'menuEditFormHead'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'menuEdit'
'-----------------------------------------------------------------------------'
'Zur Auswahl der Datenbank und zur Vorbereitung f�r menuEditFormXXXX'
'qryChange = head oder punkt f�r die DB'
'qryDetail f�r den Datensatz'
'-----------------------------------------------------------------------------'
Sub menuEdit(qryChange,qryDetail)
'erst DB festlegen '
'Menuk�pfe'
Dim h_id,h_titel,h_link,h_ordnung,h_head,h_seite

if qryChange="head" then 
   'Recordset �ffnen'
   strSQL="select * from layout_navigation_head where id=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   h_seite=objRS("seite")
   h_titel=objRS("name")
   h_ordnung=objRS("rang")
   objRS.Close
   
   Call menuEditFormHead(h_titel,h_ordnung,qryDetail,h_seite)
end if

'Menupunkte'
if qryChange="punkt" then 
   'Recordset �ffnen'
   strSQL="select * from layout_navigation where index=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   h_titel=objRS("titel")
   h_link=objRS("link")
   h_ordnung=objRS("ordnung")
   h_head=objRS("head")
   objRS.Close
   Call menuEditFormPunkt(h_titel,h_link,h_ordnung,h_head,qryDetail)
end if
      

end Sub
'-----------------------------------------------------------------------------'
'menuEdit'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
'Erzeugt Navigationsleiste links'
'DB layout_navigation und layout_navigation_head'
'-----------------------------------------------------------------------------'
Sub navigation
    Dim rang
    Dim ordnung
    rang=1
    ordnung=1
    
    strSQL="SELECT layout_navigation.*, layout_navigation_head.* FROM layout_navigation_head INNER JOIN layout_navigation ON layout_navigation_head.id = layout_navigation.head ORDER BY layout_navigation_head.id;"
    'strSQL="SELECT layout_navigation.*, layout_navigation_head.*, layout_navigation_head.id FROM layout_navigation_head INNER JOIN layout_navigation ON layout_navigation_head.id = layout_navigation.head ORDER BY layout_navigation_head.id;" 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
  	call Paint_Tab_Start
	if not objRS.EOF then
    
	    menuKopf=""
	    start=0
		    Do until objRS.EOF
				if menuKopf<>objRS("name") then
		        	    menuKopf=objRS("name")
									call Paint_Tab_Firstrow
	                                call Paint_Tab_Row("<b>"&objRS("name")&"</b>")	
									call Paint_Tab_Action("?change=head&action=edit&detail=",objRS("id"),"edit","Edit")
									call Paint_Tab_Row("-")	
									call Paint_Tab_Action("?change=head&action=new&detail=",rang,"new","Neu")									
									call Paint_Tab_Row("Rang :" & objRS("rang"))	
									call Paint_Tab_Row("Kopf ID :" & objRS("id"))	
                                    call Paint_Tab_Row("Seite :" & objRS("seite"))	
									call Paint_Tab_lastRow	
                                    rang=objRS("rang")
		                 end if
									call Paint_Tab_Firstrow
									call Paint_Tab_Row("....." & objRS("titel"))	
							        call Paint_Tab_Action("?change=punkt&action=edit&detail=",objRS("index"),"edit","Edit")									
							        call Paint_Tab_Action("?change=punkt&action=kill&detail=",objRS("index"),"del","Loeschen")																		
									call Paint_Tab_Action("?change=punkt&action=new&detail=",ordnung,"new","Neu")																		
                                    call Paint_Tab_Row("Rang :" & objRS("ordnung"))
                                    call Paint_Tab_Row(objRS("link"))
									call Paint_Tab_Row("-")	
                                    call Paint_Tab_lastRow	
                                    ordnung=objRS("ordnung")
		    objRS.MoveNext
		    Loop
	end if
    objRS.Close
    call Paint_Tab_End 
end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%
Dim qryAction
Dim qryChange
Dim qryDetail
Dim qrySenden
call Edit_Intro

qryAction=request("action")
qryChange=request("change")
qryDetail=request("detail")
qrySenden=request("senden")
'erst Action auswertern - nach Edit, New, Kill Sub mit Change �bergabe zur Trennnung zwischen MenuPunkten und K�pfen
'Detail je nach Action auswerten
if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call menuEdit(qryChange,qryDetail)
        Case "kill"    
            Call menuKill(qryChange,qryDetail)
        Case "new"    
            Call menuNew(qryChange,qryDetail)
        Case "Senden"
            Call addDBMenu(qryChange,qryDetail)        
end select

CALL navigation
%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>