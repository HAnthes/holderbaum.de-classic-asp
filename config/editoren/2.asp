<%Option Explicit

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where id=" & qryDetail & ";"
    strSQL="select * from layout_section" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


Sub addDB(qryDetail)        
Dim strWhere
    if qryDetail="" then 
      strWhere=";"
    else
      strWhere=" where id=" & qryDetail & ";"
    end if 
    strSQL="select * from layout_section" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("gruppe")=request("gruppe")
	objRS("bild")=request("bild")
    objRS.UpDate
    objRS.Close
end Sub




Sub EditForm(strBezeichnung,strBild,intID)

%>
<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">
<tr>
<td bgcolor="#BBCCFF">Bezeichnung:</td><td><input name="gruppe" size="40" value="<%=strBezeichnung%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Bild:</td><td><input name="bild" size="40" value="<%=strBild%>"></td>
</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="2"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</table>
<%
end Sub




Sub dbEdit(qryDetail)
Dim strBezeichnung,strBild,intID
   strSQL="select * from layout_section where id=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strBezeichnung=objRS("gruppe")
   strBild = objRS("Bild")
   intID=qryDetail
   objRS.Close
   Call EditForm(strBezeichnung,strBild,intID)
end Sub



Sub PaintListe
    
    strSQL="SELECT * from layout_section;"

    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
	call Paint_Tab_Start
  	if not objRS.EOF then
           Do until objRS.EOF
			   call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("id"),"edit","Edit")
			   call Paint_Tab_Row(objRS("gruppe"))	
			   call Paint_Tab_Row(objRS("Bild"))	
			   call Paint_Tab_Action("?action=kill&detail=",objRS("id"),"del","Loeschen")
			   call Paint_Tab_Lastrow                              
               objRS.MoveNext
           Loop
	end if
    objRS.Close
    call Paint_Tab_End 
end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>
<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro

Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call dbEdit(qryDetail)
			CALL PaintListe
		case "kill"
			Call killQ(qryDetail)
        Case "kill2"    
            Call killDB(qryDetail)
            Call EditForm("","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)  
            Call EditForm("","","")
			CALL PaintListe
        Case else
            Call EditForm("","","")
			CALL PaintListe
end select


%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>