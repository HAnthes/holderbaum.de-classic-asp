<%Option Explicit

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where m_index=" & qryDetail & ";"
    strSQL="select * from layout_mitarbeiter" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


Sub addDB(qryDetail)
Dim strWhere
    if qryDetail="" then
      strWhere=";"
    else
      strWhere=" where m_index=" & qryDetail & ";"
    end if
    strSQL="select * from layout_mitarbeiter" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
       objRS("m_name")=request("m_name")
       objRS("m_telefon")=request("m_telefon")
       objRS("m_email")=request("m_email")
       objRS("m_aufgabe")=request("m_aufgabe")
       objRS("m_abteilung")=request("m_abteilung")
       objRS("m_fax")=request("m_fax")
       objRS("m_beschreibung")=request("m_beschreibung")
       objRS("m_bild")=request("picit")
	   objRS("m_sammelmail")=request("strSmail")
       objRS.UpDate
       objRS.Close
end Sub




Sub EditForm(strName,strTelefon,strMail,strAufgabe,strAbteilung,strFAX,strBeschreibung,strBild,intID,strSmail)

%>
<script type="text/javascript">
function PictureWindows() {
 MyPictures = window.open("../editoren/pic.asp", "pictures", "width=600,height=500,scrollbars");
 MyPictures.focus();
}
</script>

<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">
<tr>
<td bgcolor="#BBCCFF">Name:</td><td><input name="m_name" size="40" value="<%=strName%>"></td>
<td bgcolor="#BBCCFF">Telefon:</td><td><input name="m_telefon" size="40" value="<%=strTelefon%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Aufgabe:</td><td><input name="m_aufgabe" size="40" value="<%=strAufgabe%>"></td>
<td bgcolor="#BBCCFF">Abteilung:</td><td><input name="m_abteilung" size="40" value="<%=strAbteilung%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Mail:</td><td><input name="m_email" size="40" value="<%=strMail%>"></td>
<td bgcolor="#BBCCFF">FAX:</td><td><input name="m_fax" size="40" value="<%=strFax%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Sammelmail:</td><td><input name="strSmail" size="40" value="<%=strSmail%>"></td>
<td></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Bild:</td><td><input name="picit" size="40" value="<%if strBild<>"" then response.write Server.HTMLEncode(strBild)%>"></td>
<%if strBild<>"" then %>
<td bgcolor="#BBCCFF">Bild:</td><td><%=strBild%></td>
<% else %>
<td bgcolor="#BBCCFF"> NoPic</td>
<% end if %>
<td bgcolor="#BBCCFF" colspan="2"><input type="button" value="Bilder" onClick="PictureWindows();"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF" colspan="4">Beschreibung</td>
</tr>
<tr>
<td colspan="4"><textarea rows="5" cols="60" name="m_beschreibung"><%=strBeschreibung%></textarea></td>
</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="4"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</table>
<%
end Sub




Sub dbEdit(qryDetail)
Dim strName,strTelefon,strMail,strAufgabe,strAbteilung,strFAX,strBeschreibung,strBild,intID,strSmail
   strSQL="select * from layout_mitarbeiter where m_index=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strName=objRS("m_name")
   strTelefon=objRS("m_telefon")
   strMail=objRS("m_email")
   strAufgabe=objRS("m_aufgabe")
   strAbteilung=objRS("m_abteilung")
   strFAX=objRS("m_fax")
   strBeschreibung=objRS("m_beschreibung")
   strBild=objRS("m_bild")
   strSmail = objRS("m_sammelmail")
   intID=qryDetail
   objRS.Close
   Call EditForm(strName,strTelefon,strMail,strAufgabe,strAbteilung,strFAX,strBeschreibung,strBild,intID,strSmail)
end Sub



Sub PaintListe

    strSQL="SELECT * from layout_mitarbeiter;"

    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1

	call Paint_Tab_Start

  	if not objRS.EOF then
           Do until objRS.EOF
			   call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("m_index"),"edit","Edit")
			   call Paint_Tab_Row(objRS("m_name"))
			   call Paint_Tab_Row(objRS("m_telefon"))
			   call Paint_Tab_Row(objRS("m_email"))
			   call Paint_Tab_Row(objRS("m_aufgabe"))
			   call Paint_Tab_Row(objRS("m_abteilung") & "/" & objRS("m_sammelmail") )
			   call Paint_Tab_Action("?action=kill&detail=",objRS("m_index"),"del","Loeschen")
               call Paint_Tab_Lastrow
               objRS.MoveNext
           Loop
	end if
    objRS.Close
call Paint_Tab_End

end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->

<%
Dim qryAction
Dim qryDetail
Dim qrySenden
call Edit_Intro

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call dbEdit(qryDetail)
			CALL PaintListe
		Case "kill"	
			Call killQ(qryDetail)
        Case "kill2"
            Call killDB(qryDetail)
            Call EditForm("","","","","","","","","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)
            Call EditForm("","","","","","","","","","")
			CALL PaintListe
        Case else
            Call EditForm("","","","","","","","","","")
			CALL PaintListe
end select

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>