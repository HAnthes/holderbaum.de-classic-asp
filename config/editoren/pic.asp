<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<%
'18.01.06 An neues CSS Modell angepasst
Dim strPath
Dim objFSO
Dim objFolder
Dim objItem
Dim antwortfeld


if request("real")="yes" then nurnamen = 1
'kompatiebel modus f�r die alten Editoren der alten gerneration'
'neu Editoren �bergeben Feldnamen f�r die Antwort �ber "field"
if request("field")="" then
   antwortfeld="picit"
 else
   antwortfeld=request("field")
end if


'StartPfad einstellen, bzw Pfadparameterauswerten
'Pic_Root in v1_parameter

strPath = Pic_Root

if request("pfad")<>"" then
         strPath = request("pfad")
end if
%>

<html>
<head>
<title>Bilderbuch V1.2</title>
<meta http-equiv="Content-language" content="de"/>
<link rel="stylesheet" type="text/css" href="../edit.css" media="screen" title="Edit"/>
</head>
<body>
<script language="Javascript" type="text/javascript">
<% if request("real")="yes" then %>
function sendpic(bildlink)
  {
    var buffer='';
     opener.document.Formular.<%=antwortfeld%>.value=opener.document.Formular.<%=antwortfeld%>.value+bildlink;
    return true;
 }
<%
else
%>
  function sendpic(bildlink)
  {
    var buffer='';
    var pbuffer=document.bilder.position.value;
    var endbuffer='</p>';


    if(document.bilder.text.value != "")
               {
                  pbuffer= pbuffer+'_t'
                  endbuffer='<br />' + document.bilder.text.value + '</p>'
               }
    switch(document.bilder.position.value)
    {
    case "links":
              buffer='<p class="' + document.bilder.position.value +'"><img src="' +  bildlink +'"></p>';
     break;
     case "center":
              buffer='<p class="' + document.bilder.position.value +'"><img src="' +  bildlink +'"></p>';
     break;
     case "rechts":
              buffer='<p class="' + document.bilder.position.value +'"><img src="' +  bildlink +'"></p>';
     break;
    }
     opener.document.Formular.<%=antwortfeld%>.value=opener.document.Formular.<%=antwortfeld%>.value+buffer;
     document.bilder.text.value =""
    return true;
   }
<% end if %>
</script>
<form name="bilder">
<TABLE>
    <TR>
        <TD bgcolor="#BBCCFF">
            <A HREF="<%=Request.ServerVariables("SCRIPT_NAME")&"?pfad="&root&""%>"><img src="../etc/up.gif" alt="Start Pfad" border="0">Start Pfad</a>
        </td>
        <td bgcolor="#BBCCFF">Position</td>
        <TD bgcolor="#BBCCFF">
           <select name="position" size="1">
             <option value="links">freistehend links</option>
             <option value="mittig">freistehend mitte</option>
             <option value="rechts">freistehend rechts</option>
           </select>
        </TD>
        <td bgcolor="#BBCCFF">Begleittext</td>
        <TD bgcolor="#BBCCFF"><input name="text" size="20" ></TD>
    </tr>

<%
Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
%>
    <tr>
        <td bgcolor="#BBCCFF">Name</td>
        <td bgcolor="#BBCCFF">Gr�sse</td>
        <td bgcolor="#BBCCFF">Erstellt</td>
        <td bgcolor="#BBCCFF">Type</td>
        <td bgcolor="#BBCCFF"></td>
    </tr>
<%
For Each objItem In objFolder.SubFolders
'Systemordner ausblenden
If InStr(1, objItem, "_vti", 1) = 0 Then
%>
    <tr bgcolor="#E1E1E1" onMouseOver="this.bgColor='#AAFFEE'" onMouseOut="this.bgColor='#E1E1E1'">
        <TD><A HREF="<%=Request.ServerVariables("SCRIPT_NAME")&"?field="& antwortfeld & "&real=" & request("real") &"&pfad="&strPath&objItem.Name&"/"%>"><%=objItem.Name%></A></TD>
		<TD> - </TD>
		<TD><%= objItem.DateCreated %></TD>
		<TD><%= objItem.Type %></TD>
        <td> - </td>
	</TR>
	<%
	End If
Next 'objItem

For Each objItem In objFolder.Files
	%>
	<tr bgcolor="#E1E1E1" onMouseOver="this.bgColor='#AAFFEE'" onMouseOut="this.bgColor='#E1E1E1'">
        <TD><img src="<%= strPath & objItem.Name %>" width="64"></TD>
		<TD><%= FormatNumber(objItem.Size/1024,0,-1,0,-1)  %>kB</TD>
		<TD><%= objItem.DateCreated %></TD>
		<TD><%= objItem.Type %></TD>
        <TD><input type="button" value="Setzen" onClick="sendpic('<%=strPath & objItem.Name%>');"></td>
	</TR>
	<%
Next 'objItem


Set objItem = Nothing
Set objFolder = Nothing
Set objFSO = Nothing

%>
</TABLE>
</form>
</body>
</html>