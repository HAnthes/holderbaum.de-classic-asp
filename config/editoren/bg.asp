<%Option Explicit


Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where id=" & qryDetail & ";"
    strSQL="select * from layout_bg" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


Sub addDB(qryDetail)        
Dim strWhere
    if qryDetail="" then 
      strWhere=";"
    else
      strWhere=" where id=" & qryDetail & ";"
    end if 
    strSQL="select * from layout_bg" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("bild")=request("bild")
	objRS("datumStart")=request("datumStart")
	objRS("datumEnde")=request("datumEnde")
	objRS("xy")=request("xy")
    objRS.UpDate
    objRS.Close
end Sub




Sub EditForm(bild,datumStart,datumEnde,xy, intID)

%>
<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">
<tr>
<td bgcolor="#BBCCFF">Bild:</td><td><input name="bild" size="50" value="<%=bild%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Start Datum:</td><td><input name="datumStart" size="50" value="<%=datumStart%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">End Datum:</td><td><input name="datumEnde" size="50" value="<%=datumEnde%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">JS Config XY:</td><td><input name="xy" size="50" value="<%=xy%>"></td>
</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="2"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</table>
<%
end Sub




Sub dbEdit(qryDetail)
Dim bild,datumStart,datumEnde,xy, intID
   strSQL="select * from layout_bg where id=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   bild=objRS("bild")
   datumStart=objRS("datumStart")
   datumEnde=objRS("datumEnde")
   xy=objRS("xy")
   intID=qryDetail
   objRS.Close
   Call EditForm(bild,datumStart,datumEnde,xy, intID)
end Sub



Sub PaintListe
    strSQL="SELECT * from layout_bg order by datumStart;"
	Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
	call Paint_Tab_Start
  	if not objRS.EOF then
           Do until objRS.EOF
			   call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("id"),"edit","Edit")
			   call Paint_Tab_Row(objRS("bild"))	
			   call Paint_Tab_Row(objRS("datumStart"))	
			   call Paint_Tab_Row(objRS("datumEnde"))	
			   call Paint_Tab_Row(objRS("xy"))	
			   call Paint_Tab_Action("?action=kill&detail=",objRS("id"),"del","Loeschen")
			   call Paint_Tab_Lastrow                              
               objRS.MoveNext
           Loop
	end if
    objRS.Close
    call Paint_Tab_End 
end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>
<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro

Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call dbEdit(qryDetail)
			CALL PaintListe
		case "kill"
			Call killQ(qryDetail)
        Case "kill2"    
            Call killDB(qryDetail)
            Call EditForm("","","","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)  
            Call EditForm("","","","","")
			CALL PaintListe
        Case else
            Call EditForm("","","","","")
			CALL PaintListe
end select


%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>