<%
'18.08.05 Editor f�r die News-Eintr�ge.
'18.08.05 neues Layout
'29.11.05 Prototype einer Sicherheitsabfrage beim l�schen - funktion killq in edit_intro.asp
'26.01.07 Bereinigt, Linkerfunktionen raus. Die Popup f�r den Ersetzer eingbaut

Option Explicit

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where n_index=" & qryDetail & ";"
    strSQL="select * from layout_news" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub

Sub addDB(qryDetail)
'ist qryDetail gegeben dann ein alter DS ist nicht dann eine Neuer'
'zuerst where'
Dim strWhere
    if qryDetail="" then
      strWhere=";"
    else
      strWhere=" where n_index=" & qryDetail & ";"
    end if
    strSQL="select * from layout_news" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("news_kopf")=request("newskopft")
    objRS("gruppe")=request("gruppe")
	
    objRS("news")=request("picit")
    objRS("erweitert")=request("erweitert")
    if qryDetail="" then objRS("n_datum")=date()
	'F�r einen Sticky News Eintrag....
	if request("gruppe") = 10 then objRS("n_datum") = "31.12.2085"
    objRS.UpDate
    objRS.Close
end Sub


'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'
'Formular f�r die Daten�nderung '
'----------------------------------------------------------------------------'
Sub NewsEditForm(strNewsKopf,intGruppe,strNews,strErweitert,intID)

%>
<script type="text/javascript">

function PictureWindows() {
 MyPictures = window.open("../editoren/pic.asp?field=picit", "pictures", "width=600,height=500,scrollbars");
 MyPictures.focus();
}

function litext()
  {
   MyLiText = window.open("../editoren/linker.asp?field=picit", "linker", "width=600,height=500,scrollbars");
   MyLiText.focus();
}
</script>

<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">

<tr>
<td bgcolor="#BBCCFF">�berschrift</td><td><input name="newskopft" size="80" value="<%=strNewsKopf%>"></td>
<td ><select name="gruppe" size="1">
		<%
			'Dropdown f�r K�pfe
			strSQL="select * from layout_section;"
			Set objRS = Server.CreateObject("ADODB.Recordset")
			objRS.Open strSQL,objConn, 1, 1
			if not objRS.EOF then
				Do until objRS.EOF
					response.write "<option value=""" & objRS("id") & """"
					if intGruppe=objRS("id") then
						response.write " selected "
					end if
					response.write ">" & objRS("gruppe") & "</option>"
					objRS.MoveNext
				Loop
			end if
			objRS.Close
			%>
</select></td></tr>

<tr>
<td bgcolor="#BBCCFF" >Zus�tze</td>
<td><input type="button" value="Bilder" onClick="PictureWindows();"><input type="button" value="K�rzel" onClick="litext();"></td>
<td></td>
</tr><tr><td bgcolor="#BBCCFF" colspan="3">News</td>
</tr><tr>
<td colspan="3"><textarea rows="6" cols="60" name="picit"><%=strNews%></textarea></td>
</tr><tr><td bgcolor="#BBCCFF" colspan="3">erweitert</td>
</tr><tr>
<td colspan="3"><textarea rows="6" cols="60" name="erweitert"><%=strErweitert%></textarea></td>
</tr><tr>
<td bgcolor="#DDFFAA" colspan="3"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</tr>
</table>
<%
end Sub
'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'NewsEdit'
'-----------------------------------------------------------------------------'
'Zur Auswahl der Datenbank und zur Vorbereitung f�r menuEditFormXXXX'
'qryChange = head oder punkt f�r die DB'
'qryDetail f�r den Datensatz'
'-----------------------------------------------------------------------------'

Sub NewsEdit(qryDetail)
Dim strNewsKopf,intGruppe,strNews,strErweitert,intID
   strSQL="select * from layout_news where n_index=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strNewsKopf=objRS("news_kopf")
   intGruppe=objRS("gruppe")
   strNews=objRS("news")
   strErweitert=objRS("erweitert")
   intID=qryDetail
   objRS.Close
   Call NewsEditForm(strNewsKopf,intGruppe,strNews,strErweitert,intID)
end Sub
'-----------------------------------------------------------------------------'
'menuEdit'
'-----------------------------------------------------------------------------'



'-----------------------------------------------------------------------------'
'Anzeigen der Daten'
'strSQL und einzelen Felder �nderbar'
'-----------------------------------------------------------------------------'
Sub PaintListe

'SQL Parameter f�r die Abfrage und Listenaufbau
strSQL="SELECT layout_news.n_index, layout_news.news_kopf, layout_news.News, layout_news.erweitert, layout_news.n_datum, layout_section.gruppe, layout_section.Bild, * FROM layout_news INNER JOIN layout_section ON layout_news.gruppe = layout_section.id ORDER BY layout_news.n_datum desc;"

'Datenbankverbindung herstellen
Set objRS = Server.CreateObject("ADODB.Recordset")
objRS.Open strSQL,objConn, 1, 1

'Datemholen und Anzeigen
'Tabelle einleiten
call Paint_Tab_Start

if not objRS.EOF then
           Do until objRS.EOF
		       call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("n_index"),"edit","Edit")
			   call Paint_Tab_Row(objRS("news_kopf"))
			   call Paint_Tab_Row(objRS("layout_section.gruppe"))
			   call Paint_Tab_Row(objRS("n_datum"))
			   call Paint_Tab_Action("?action=kill&detail=",objRS("n_index"),"del","Loeschen")
               call Paint_Tab_Lastrow
               objRS.MoveNext
           Loop
	end if
    objRS.Close
call Paint_Tab_End
end sub
'-----------------------------------------------------------------------------'
'Anzeigen der Daten'
'-----------------------------------------------------------------------------'
%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro

Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call NewsEdit(qryDetail)
			CALL PaintListe
        Case "kill"
            Call killQ(qryDetail)
        Case "kill2"    
			Call killdb(qryDetail)
			Call NewsEditForm("","","","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)
            Call NewsEditForm("","","","","")
			CALL PaintListe
        Case else
            Call NewsEditForm("","","","","")
			CALL PaintListe
end select

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>