<%Option Explicit

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where c_index=" & qryDetail & ";"
    strSQL="select * from layout_content" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


sub zero_counter(qryDetail)
      strSQL="select * from layout_content where c_index=" & qryDetail & ";"
	  Set objRS = Server.CreateObject("ADODB.Recordset")
	  objRS.Open strSQL,objConn, 1, &H0002
      objRS("c_counter")=0
	  objRS("c_datum")=date()
	  objRS.UpDate
      objRS.Close
end Sub	  
	   
Sub addDB(qryDetail)
'ist qryDetail gegeben dann ein alter DS ist nicht dann eine Neuer'
'zuerst where'
Dim strWhere
    if qryDetail="" then
      strWhere=";"
    else
      strWhere=" where c_index=" & qryDetail & ";"
    end if
    strSQL="select * from layout_content" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("c_bezeichnung")=request("bezeichnung")
    objRS("c_inhalt")=request("picit")
    objRS("c_html")=request("html")
	objRS("c_keywords")=request("keywords")
    if qryDetail="" then
       objRS("c_datum")=date()
       objRS("c_counter")=0
    end if
    objRS.UpDate
    objRS.Close
end Sub


'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'
'Formular f�r die Daten�nderung '
'----------------------------------------------------------------------------'
Sub EditForm(strBezeichnung,strInhalt,strHTML,strKeyWords,intID)

%>
<script type="text/javascript">

function PictureWindows() {
 MyPictures = window.open("../editoren/pic.asp", "pictures", "width=600,height=500,scrollbars");
 MyPictures.focus();
}
function preview() {
 MyPreView = window.open("../../default.asp?action=con&detail=<%=intID%>","","") ;
 MyPreView.focus();
}

function litext()
  {
   MyLiText = window.open("../editoren/linker.asp?field=picit", "linker", "width=600,height=500,scrollbars");
   MyLiText.focus();
}
</script>

<table width="100%">

<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">

<tr>
<td bgcolor="#BBCCFF">Bezeichnung:</td>
<td ><input name="bezeichnung" size="40" value="<%=strBezeichnung%>"><input name="html" size="10" value="<%=strHTML%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">KeyWords:</td>
<td><textarea rows="3" cols="80" name="keywords"><%=strKeyWords%></textarea></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Links</td><td>
<input type="button" value="Bilder" onClick="PictureWindows();">
<input type="button" value="K�rzel" onClick="litext();"></td>
</td></tr>
<tr><td bgcolor="#BBCCFF" colspan="2">Inhalt</td>
</tr><tr>
<td colspan="2"><textarea rows="30" cols="80" name="picit"><%=Server.HTMLEncode(strInhalt)%></textarea></td>
</tr><tr>
<td bgcolor="#DDFFAA"><input name="senden" type="submit" value="Senden" ></td>
<td bgcolor="#DDFFAA" colspan="2"><input type="button" value="Vorschau" onClick="preview();"></td>
</tr>
</form>
</tr>
</table>
<%
end Sub
'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'NewsEdit'
'-----------------------------------------------------------------------------'
'Zur Auswahl der Datenbank und zur Vorbereitung f�r menuEditFormXXXX'
'qryChange = head oder punkt f�r die DB'
'qryDetail f�r den Datensatz'
'-----------------------------------------------------------------------------'

Sub dbEdit(qryDetail)
Dim strBezeichnung,strInhalt,strHTML,intID,strKeyWords
   strSQL="select * from layout_content where c_index=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strBezeichnung=objRS("c_Bezeichnung")
   strInhalt=objRS("c_inhalt")
   strHTML=objRS("c_html")
   intID=qryDetail
   strKeyWords=objRS("c_keywords")
   objRS.Close
   Call EditForm(strBezeichnung,strInhalt,strHTML,strKeyWords,intID)
end Sub
'-----------------------------------------------------------------------------'
'menuEdit'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
'Erzeugt Navigationsleiste links'
'DB layout_navigation und layout_navigation_head'
'-----------------------------------------------------------------------------'
Sub PaintListe

    strSQL="SELECT * from layout_content;"

    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1
call Paint_Tab_Start

  	if not objRS.EOF then
           Do until objRS.EOF
		       call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("c_index"),"edit","Edit")
			   call Paint_Tab_Row(objRS("c_bezeichnung"))
			   if date()= objRS("c_datum") then
			   			   call Paint_Tab_Row("* " & objRS("c_datum"))
				else
						   call Paint_Tab_Row(objRS("c_datum"))
				end if 
			   call Paint_Tab_Row(objRS("c_counter"))
			   if objRS("c_counter")=0 then
					call Paint_Tab_Action("?action=zero&detail=",objRS("c_index"),"o","Zero")
			   else
					call Paint_Tab_Action("?action=zero&detail=",objRS("c_index"),"p","Zero")
			   end if 
			   call Paint_Tab_Action("?action=kill&detail=",objRS("c_index"),"del","Loeschen")
               call Paint_Tab_Lastrow
               objRS.MoveNext
           Loop
	end if
    objRS.Close
call Paint_Tab_End


end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>
<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%
Dim qryAction
Dim qryDetail
Dim qrySenden
call Edit_Intro

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call dbEdit(qryDetail)
			CALL PaintListe
		Case "kill"	
			Call killQ(qryDetail)
        Case "kill2"
            Call killDB(qryDetail)
            Call EditForm("","","","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)
            Call EditForm("","","","","")
			CALL PaintListe
       Case "zero"
            Call zero_counter(qryDetail)
            Call EditForm("","","","","")
			CALL PaintListe
	   Case else
            Call EditForm("","","","","")
			CALL PaintListe
end select

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>