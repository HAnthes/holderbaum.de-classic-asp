<%Option Explicit
'26.10.04 Anthes. Status als Textfeld hinzugefügt'

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where id=" & qryDetail & ";"
    strSQL="select * from cms_user" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


Sub addDB(qryDetail)        
Dim strWhere
    if qryDetail="" then 
      strWhere=";"
    else
      strWhere=" where id=" & qryDetail & ";"
    end if 
    strSQL="select * from cms_user" & strWhere 
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    if qryDetail="" then objRS.AddNew
    objRS("name")=request("name")
    objRS("fullname")=request("fullname")
    objRS("email")=request("email")
    objRS("status")=request("status")
    if request("pw") <>"" then
         objRS("pw")=sha256(request("pw"))
    end if 
    objRS.UpDate
    objRS.Close
end Sub




Sub EditForm(strName,strFullName,strEMail,strPW,intID,strStatus)

%>
<table width="100%">
<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?detail=<%=intId%>" method="post" name="Formular">
<tr>
<td bgcolor="#BBCCFF">Name:</td><td ><input name="name" size="40" value="<%=strName%>"></td>
<td bgcolor="#BBCCFF">PW:</td><td ><input name="pw" size="40"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Full_Name:</td><td><input name="fullname" size="40" value="<%=strFullName%>"></td>
<td bgcolor="#BBCCFF">E-Mail:</td><td><input name="email" size="40" value="<%=strEMail%>"></td>
</tr>
<tr>
<td bgcolor="#BBCCFF">Status:</td><td colspan="3"><input name="status" size="20" value="<%=strStatus%>"></td>

</tr>
<tr>
<td bgcolor="#DDFFAA" colspan="4"><input name="senden" type="submit" value="Senden" ></td>
</tr>
</form>
</table>
<%
end Sub




Sub dbEdit(qryDetail)
Dim strName,strPW,strFullName,strEMail,intID,strStatus
   strSQL="select * from cms_user where id=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strName=objRS("name")
   strFullName=objRS("fullname")
   strEMail=objRS("email")
   strPW=objRS("pw")
   strStatus=objRS("status")
   intID=qryDetail
   objRS.Close
   Call EditForm(strName,strFullname,strEMail,strPW,intID,strStatus)
end Sub



Sub PaintListe
    
    strSQL="SELECT * from cms_user;"

    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, 1

call Paint_Tab_Start

if not objRS.EOF then
           Do until objRS.EOF
		       call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("id"),"edit","Edit")
			   call Paint_Tab_Row(objRS("name"))	
			   call Paint_Tab_Row(objRS("fullname"))	
			   call Paint_Tab_Row(objRS("email"))	
			   call Paint_Tab_Row(objRS("status"))	
			   call Paint_Tab_Row(objRS("lastlogin_d")& " - " &objRS("lastlogin_z"))	
			   call Paint_Tab_Action("?action=kill&detail=",objRS("id"),"del","Loeschen")
               call Paint_Tab_Lastrow
               objRS.MoveNext
           Loop
	end if
    objRS.Close
call Paint_Tab_End 
end sub

'-----------------------------------------------------------------------------'
'Navigationsleiste'
'-----------------------------------------------------------------------------'
%>
<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../../-Vscripts/inc_sha256.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro
Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
            Call dbEdit(qryDetail)
			CALL PaintListe
		Case "kill"
			Call killQ(qryDetail)
        Case "kill2"    
            Call killDB(qryDetail)
            Call EditForm("","","","","","")
			CALL PaintListe
        Case "Senden"
            Call addDB(qryDetail)  
            Call EditForm("","","","","","")
			CALL PaintListe
        Case else
            Call EditForm("","","","","","")
			CALL PaintListe
end select

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>