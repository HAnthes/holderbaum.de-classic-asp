<%
'23.08.05 Z�hler erweitert, Anzeige von Tages und Gesamtcounter und erstem Klick.
'01.09.05 Durchschnitt Click ab ersten eingef�gt
'23.11.05 Z�hler f�r Fahrzeuge mit Bildern 
'F�r die Bildererkennung ben�tigte Funktionen
'30.11.05 Tabellen �berschrift
'30.11.05 Standtage anstatt ersterklick (Standtage ab ersten Klick)
'17.02.06 Spalten f�r GW-Nr und Interne Nr (zb. Bilder Links oder Mobile verweis)
'07.12.07 Ersteseitebuchung Mobileschnittstelle Nov 2007
'28.01.10 Finanzierungs feature aus Mobile schnittstelle �bernehmen. (finfeature) 
'28.05.10 Topinserat mobile.
'31.05.2011 : Finanzierung wird Youtube geopfert

Function FileExists(strFileName)
Dim FileName, fs
  set fs=Server.CreateObject("Scripting.FileSystemObject")
    FileName = Server.MapPath(strFileName)
    if fs.FileExists(Filename) then
        FileExists = True
    else
        FileExists = False
    end if
    set fs=nothing
End Function

function check_pictures(Pic_str)
	Dim Small_Pic
	Small_Pic=pic_root & "fzg/small/"
	check_pictures=fileexists(Small_Pic & Pic_str &"_1.jpg")
end function
'Bildererkennung


'Erstellen eines l�schauftrages
'�berfl�ssig mit neuer Bilderverwaltung durch des FTP Exportmodul
sub BilderLoschen(intDBID)
    strSQL = "select * from car_del"
	Set objRS = Server.CreateObject("ADODB.Recordset")
	objRS.Open strSQL,objConn, 1, &H0002
	objRS.AddNew
	objRS("ser_id") = intDBID
	objRS.Update
	objRS.Close
end sub

'�berfl�ssig mit neuer Bilderverwaltung durch des FTP Exportmodul
'Storno eines L�schauftrages
sub StornoBilderLoschen(intDBID)
	strSQL = "select * from car_del where ser_id= " &intDBID & ";"
	Set objRS = Server.CreateObject("ADODB.Recordset")
	objRS.Open strSQL,objConn, 1, &H0002
	if not objRS.EOF then
      objRS.Delete
      objRS.Update
      objRS.Close
    end if
end sub


'L�schen der Finanzierungsdaten
sub killDB(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if not objRS.EOF then
     objRS("anz")=0
     objRS("lauf")=0
     objRS("rest")=0
     objRS("eff")=0
     objRS("mtl")=0
	 'Finfeature dann immer auf 0 setzen!
	 objRS("finfeature")=0
     objRS.Update
     objRS.Close
  end if
end sub


'Eintragen der Finanzierungsdaten
sub addDB(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
     objRS("seri") = intDBID
     objRS("anz")=request("anz")
     objRS("lauf")=request("lauf")
     objRS("rest")=request("rest")
     objRS("eff")=request("eff")
     objRS("mtl")=request("mtl")
     objRS.Update
  objRS.Close
end sub

sub youtube(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
     objRS("seri") = intDBID
	 help  = request("yl") 
	 if instr(help,"/watch?v=") >0 then help = replace(help, "/watch?v=", "/embed/")
     objRS("youtubelink")=help
     objRS("youtubeobj")=request("yo")
     objRS.Update
  objRS.Close
end sub

'Blickf�nger f�r Mobile sonderfunktion
sub mobbli(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
  objRS("mobile_blickfang")=1
  objRS("seri")=intDBID
  objRS.Update
  objRS.Close
end sub

sub mobblix(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  objRS("mobile_blickfang")=0
  objRS.Update
  objRS.Close
end sub
'Blickf�nger f�r Mobile sonderfunktion

'Ersteseite Buchung Mobile Sonderfunktion
sub mobfs(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
  objRS("ersteseite")=1
  objRS("seri")=intDBID
  objRS.Update
  objRS.Close
end sub

sub mobfr(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  objRS("ersteseite")=0
  objRS.Update
  objRS.Close
end sub
'Ersteseite Buchung Mobile Sonderfunktion

'Finanzierungsfeature Mobile Sonderfunktion
sub mobff(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
  objRS("finfeature")=1
  objRS("seri")=intDBID
  objRS.Update
  objRS.Close
end sub

sub mobffx(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  objRS("finfeature")=0
  objRS.Update
  objRS.Close
end sub
'Finanzierungsfeature Mobile Sonderfunktion


'Topinserat
sub mobti(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  if objRS.EOF then objRS.AddNew
  objRS("topinserat")=1
  objRS("seri")=intDBID
  objRS.Update
  objRS.Close
end sub

sub mobtix(intDBID)
  strSQL="SELECT * FROM cars_info where seri= " & intDBID & ";"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, &H0002
  objRS("topinserat")=0
  objRS.Update
  objRS.Close
end sub
'Topinserat

sub EditOld(intDBID)
%>
<table width="100%">
  <tr>
  <form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?action=add&detail=<%=intDBID%>" method="post" name="Formular">
<%
  strSQL="SELECT cars.*, cars_info.*, cars_stat.*, cars.SER_ID FROM (cars LEFT JOIN cars_info ON cars.SER_ID = cars_info.seri) LEFT JOIN cars_stat ON cars.SER_ID = cars_stat.fzgnr WHERE (((cars.SER_ID)=" & intDBID &"));"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, 1
  if not objRS.EOF then
    response.write "<td bgcolor=""#BBCCFF"">" & objRS("hersteller") & " " & objRS("modell") & "</td>"&vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">GW-NR:" & objRS("auf_nr") & "</td>"&vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">EZ:" & objRS("ez") & "</td>"&vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">kW:" & objRS("leistung") & "</td>"&vbCRLF
	if objRS("gesch_art") = "0" then
			brutto=objRS("preis")*(VarMwSt/100+1)
		else
			brutto=objRS("preis")
		end if
    response.write "<td bgcolor=""#BBCCFF"">Preis:" & FormatNumber(brutto,0,-1,0,-1) & "</td></tr>"&vbCRLF
    response.write "<tr><td bgcolor=""#BBCCFF"">Anzahlung (Ganzzahl) :<input name=""anz"" size=""10"" value=""" & objRS("anz") & """></td>" & vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">Rate (Ganzzahl) :<input name=""mtl"" size=""10"" value=""" & objRS("mtl") & """></td>" & vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">Rest (Ganzzahl) :<input name=""rest"" size=""10"" value=""" & objRS("rest") & """></td>" & vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">Laufzeit  :<input name=""lauf"" size=""10"" value=""" & objRS("lauf") & """></td>" & vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">Zins (mit ,):<input name=""eff"" size=""10"" value=""" & objRS("eff") & """></td></tr>" & vbCRLF
  end if
 objRS.Close
%>
<tr>
<td bgcolor="#DDFFAA" colspan="5"><input name="senden" type="submit" value="Senden" ></td>
</form>
</tr>
</table>
<%
end sub


sub Edit(intDBID)
%>
<table width="100%">
  <tr>
  <form action="<%=Request.ServerVariables("SCRIPT_NAME")%>?action=add&detail=<%=intDBID%>" method="post" name="Formular">
<%
  strSQL="SELECT cars.*, cars_info.*, cars_stat.*, cars.SER_ID FROM (cars LEFT JOIN cars_info ON cars.SER_ID = cars_info.seri) LEFT JOIN cars_stat ON cars.SER_ID = cars_stat.fzgnr WHERE (((cars.SER_ID)=" & intDBID &"));"
  Set objRS = Server.CreateObject("ADODB.Recordset")
  objRS.Open strSQL,objConn, 1, 1
  if not objRS.EOF then
    response.write "<td bgcolor=""#BBCCFF"">" & objRS("hersteller") & " " & objRS("modell") & "</td>"&vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">GW-NR:" & objRS("auf_nr") & "</td></tr>"&vbCRLF
	
    response.write "<tr><td bgcolor=""#BBCCFF"">You:Link :<textarea rows=""2"" cols=""60"" name=""yl"" size=""50"">" & objRS("youtubelink") & "</textarea></td>" & vbCRLF
    response.write "<td bgcolor=""#BBCCFF"">You:Obj :<textarea rows=""2"" cols=""60"" name=""yo"">" & objRS("youtubeobj") & "</textarea></td></tr>" & vbCRLF
  end if
 objRS.Close
%>
<tr>
<td bgcolor="#DDFFAA" colspan="5"><input name="senden" type="submit" value="Senden" ></td>
</form>
</tr>
</table>
<%
end sub

sub Carliste
 Dim C_cars,C_Day,C_All,C_Preis,C_Pic,C_Front,C_Blick,C_TI
 C_cars=0
 C_day=0
 C_All=0
 C_preis=0
 C_Pic=0
 C_Front=0
 C_Blick=0
 C_TI=0

 strSQL="SELECT cars.SER_ID, cars.AUF_NR, cars.hersteller, cars.modell, cars.lack, cars.preis,cars_info.youtubelink ,cars_info.lauf,cars_info.ersteseite, cars_stat.counter_view,cars_stat.front, cars_info.seri,cars_info.mobile_blickfang, car_del.ser_id AS todo, cars_stat.counter_view_old, cars_stat.first, cars_stat.tag,cars_stat.counter_time,cars_info.finfeature,cars_info.topinserat, [counter_view]+[counter_view_old] AS sortc FROM ((cars LEFT JOIN cars_info ON cars.SER_ID = cars_info.seri) LEFT JOIN cars_stat ON cars.SER_ID = cars_stat.fzgnr) LEFT JOIN car_del ON cars.SER_ID = car_del.ser_id ORDER BY [first];"
 
 Set objRS = Server.CreateObject("ADODB.Recordset")
 objRS.Open strSQL,objConn, 1, 1
 call Paint_Tab_Start
 if not objRS.EOF then
 
 call Paint_Tab_Firstrow
 call Paint_Tab_Row("YT")
 call Paint_Tab_Row("-")
 call Paint_Tab_Row("EVA ID/GW ")
 'call Paint_Tab_Row("GW-Nr")
 call Paint_Tab_Row("Fahrzeug")
 'call Paint_Tab_Row("Farbe")
 call Paint_Tab_Row("Preis")
 call Paint_Tab_Row("Heute/Time")
 call Paint_Tab_Row("Gesamt")
 call Paint_Tab_Row("Durch.")
 call Paint_Tab_Row("S.Tage")
 call Paint_Tab_Row("Front")
 call Paint_Tab_Row("B 0,2")
 call Paint_Tab_Row("S 1,2")
 call Paint_Tab_Row("F 0,1")
 call Paint_Tab_Row("T 3,0")
 call Paint_Tab_Lastrow
 
 
 Do until objRS.EOF
   
    C_cars=c_cars+1
	C_preis=C_preis+objRS("preis")
	
	call Paint_Tab_Firstrow
	call Paint_Tab_Action("?action=edit&detail=",objRS("ser_id"),"edit","Edit")
    if  len(objRS("youtubelink")) > 1 then
	   call Paint_Tab_Row("V")
    else
	   call Paint_Tab_Row("-")
    end if
	'new
	if check_pictures(objRS("ser_id")) then
		call Paint_Tab_Row("<a onClick=""window.open('/print.asp?ac="& objRS("ser_id") &"', 'Hilfe', 'width=800,height=700,scrollbars');"" ><img src=""../etc/sub.gif"">" & objRS("ser_id") & "-" & objRS("auf_nr") &"</a>")
	else
		call Paint_Tab_Row(objRS("ser_id"))
	end if
	'nnew
	'mit SEriID zusammen gelegt
	'call Paint_Tab_Row("<a onClick=""window.open('../editoren/carinfo.asp?help="& objRS("ser_id") &"', 'Hilfe', 'width=1000,height=500,scrollbars');"" ><img src=""../etc/sub.gif"">" & objRS("auf_nr") &"</a>")
	call Paint_Tab_Row("<a onClick=""window.open('/print.asp?ac="& objRS("ser_id") &"', 'Hilfe', 'width=1000,height=500,scrollbars');"" ><img src=""../etc/sub.gif"">" &objRS("hersteller") & " " & objRS("modell") &"<br>" & objRS("lack") &"</a>")
	'call Paint_Tab_Row("<a onClick=""window.open('../editoren/carinfo.asp?help="& objRS("ser_id") &"', 'Hilfe', 'width=600,height=500,scrollbars');"" ><img src=""../etc/sub.gif"">" & objRS("hersteller") & " " & objRS("modell") &"</a>")
	'call Paint_Tab_Row(objRS("lack"))
	call Paint_Tab_Row(FormatNumber(objRS("preis"),0,-1,0,-1))
	if objRS("tag") = date() then
		call Paint_Tab_Row(objRS("counter_view")&"/"&left(right(objRS("counter_time"),8),5))
	    C_All=C_All+objRS("counter_view")
	    C_day=C_day+objRS("counter_view") 
	else
	   call Paint_Tab_Row("-")
	end if 
	if objRS("first") <>"" then
		call Paint_Tab_Row(objRS("counter_view_old")+objRS("counter_view"))
		call Paint_Tab_Row("� " & FormatNumber(((objRS("counter_view_old")+ objRS("counter_view")) / (datediff("d",objRS("first"),date())+1)),2))
		C_All=C_All+objRS("counter_view_old")	
	else 
	    call Paint_Tab_Row("-")
		call Paint_Tab_Row("-")
    end if
	call Paint_Tab_Row(date()-objRS("first"))
	dstand=dstand+date()-objRS("first")
	call Paint_Tab_Row(objRS("front"))
	if isnumeric(objRS("front")) then C_Front=C_Front+objRS("front")
	'if check_pictures(objRS("ser_id")) then
	 ' C_Pic=C_Pic+1
	  'if  objRS("todo")<>"" then
	  '  call Paint_Tab_Action("?action=storno&detail=",objRS("ser_id"),"achtung","Loeschauftrag stornieren")
	  'else
       ' call Paint_Tab_Action("?action=picdel&detail=",objRS("ser_id"),"del","Loeschauftrag erteilen")
	  'end if
	'else
	' call Paint_Tab_Row(".")
    'end if
	'Mobile Blickfangsetzen / l�schen
	if not isnumeric(objRS("mobile_blickfang")) or objRS("mobile_blickfang")=0 then
		call Paint_Tab_Action("?action=ms&detail=",objRS("ser_id"),"blick","Blickfang setzen")
	else
		call Paint_Tab_Action("?action=mr&detail=",objRS("ser_id"),"Ublick","Blickfang l�schen")
		C_Blick=C_Blick+1
	end if
	'ende mobile Blickfag setzen
	'Mobile Ersrte Seite buchung
	if not isnumeric(objRS("ersteseite")) or objRS("ersteseite")=0 then
		call Paint_Tab_Action("?action=fs&detail=",objRS("ser_id"),"empf","Ersteseite buchen")
	else
		call Paint_Tab_Action("?action=fr&detail=",objRS("ser_id"),"Uempf","Ersteseite l�schen")
		C_Erste=C_Erste+1
	end if
	'Finfeatury
	if not isnumeric(objRS("finfeature")) or objRS("finfeature")=0 then
		call Paint_Tab_Action("?action=ff&detail=",objRS("ser_id"),"fin","FF buchen")
	else
		call Paint_Tab_Action("?action=ffx&detail=",objRS("ser_id"),"Ufin","FF l�schen")
		C_FF=C_FF+1
	end if
    'ende
	'Finfeatury
	if not isnumeric(objRS("topinserat")) or objRS("topinserat")=0 then
		call Paint_Tab_Action("?action=ti&detail=",objRS("ser_id"),"mti","TI buchen")
	else
		call Paint_Tab_Action("?action=tix&detail=",objRS("ser_id"),"umti","TI l�schen")
		C_TI=C_TI+1
	end if
    'ende
    call Paint_Tab_Lastrow
    objRS.MoveNext
   Loop
 end if
 objRS.Close
 call Paint_Tab_Firstrow
 'Fin +
 call Paint_Tab_Row("-")
 'Fin -
  call Paint_Tab_Row("-")
 call Paint_Tab_Row(C_cars)
 call Paint_Tab_Row("-")
 call Paint_Tab_Row(FormatNumber(c_preis,0,-1,0,-1))
 call Paint_Tab_Row(C_day)
 call Paint_Tab_Row(C_All+C_day)
 call Paint_Tab_Row("-")
 call Paint_Tab_Row("nunkn")
 call Paint_Tab_Row(C_Front &" - "& FormatNumber(100/(C_All+C_day)*C_Front,0,-1,0,-1) & "%")
 'call Paint_Tab_Row(C_pic)
 call Paint_Tab_Row(C_Blick &"/"& 0.1*C_Blick &"�")
 call Paint_Tab_Row(C_Erste &"/"& 1.2*C_Erste& "�")
 call Paint_Tab_Row(C_FF &"/"& 0.1*C_FF& "�")
 call Paint_Tab_Row(C_TI &"/"& 3*C_TI& "�")
 call Paint_Tab_Lastrow
 call Paint_Tab_End
end sub

%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro
Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")

Select Case qryAction
        Case "edit"
            Call Edit(qryDetail)
        Case "kill"
            Call killDB(qryDetail)
            Call Edit(qryDetail)
        Case "add"
            'Call addDB(qryDetail)
			Call youtube(qryDetail)
            Call Edit(qryDetail)
		'wird nicht mehr ben�tigt. L�schfunktion in FTP Modul
		case "picdel"
		     Call BilderLoschen(qryDetail)
		case "storno"
			Call StornoBilderLoschen(qryDetail)
		'wird nicht mehr ben�tigt. L�schfunktion in FTP Modul
		case "mr"
			Call mobblix(qryDetail)
		case "ms"
			Call mobbli(qryDetail)
		case "fr"
			Call mobfr(qryDetail)
		case "fs"
			Call mobfs(qryDetail)	
		case "ff"
			Call mobff(qryDetail)
		case "ffx"
			Call mobffx(qryDetail)		
		case "ti" 
			call mobti(qryDetail)
		case "tix"
			call mobtix(qryDetail)
end select
Call carliste
%>
</body>
</html>
<!--#include virtual="/-Vscripts/v1_db_close.asp" -->