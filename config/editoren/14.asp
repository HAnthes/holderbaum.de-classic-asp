<%
'18.08.05 Editor f�r die News-Eintr�ge.
'18.08.05 neues Layout
'29.11.05 Prototype einer Sicherheitsabfrage beim l�schen - funktion killq in edit_intro.asp
'26.01.07 Bereinigt, Linkerfunktionen raus. Die Popup f�r den Ersetzer eingbaut

Option Explicit

Sub killDB(qryDetail)
Dim strWhere
    strWhere=" where n_index=" & qryDetail & ";"
    strSQL="select * from layout_mail" & strWhere
    Set objRS = Server.CreateObject("ADODB.Recordset")
    objRS.Open strSQL,objConn, 1, &H0002
    objRS.Delete
    objRS.Close
end Sub


'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'
'Formular f�r die Daten�nderung '
'----------------------------------------------------------------------------'
Sub EditForm(strziel,strquelle,strmail)

%>
<table width="100%">
<tr><td bgcolor="#BBCCFF" ><%=strZiel%></td><td bgcolor="#BBCCFF" ><%=strQuelle%></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><%=strMail%></td></tr>
</table>
<%
end Sub
'-----------------------------------------------------------------------------'
'NewsEditForm'
'-----------------------------------------------------------------------------'


'-----------------------------------------------------------------------------'
'NewsEdit'
'-----------------------------------------------------------------------------'
'Zur Auswahl der Datenbank und zur Vorbereitung f�r menuEditFormXXXX'
'qryChange = head oder punkt f�r die DB'
'qryDetail f�r den Datensatz'
'-----------------------------------------------------------------------------'

Sub Edit(qryDetail)
Dim strziel,strquelle,strmail
   strSQL="select * from layout_mail where id=" & qryDetail& ";"
   Set objRS = Server.CreateObject("ADODB.Recordset")
   objRS.Open strSQL,objConn, 1, 1
   strZiel=objRS("ziel")
   strQuelle=objRS("quelle")
   strMail=objRS("text")

   objRS.Close
   Call EditForm(strziel,strquelle,strmail)
end Sub
'-----------------------------------------------------------------------------'
'menuEdit'
'-----------------------------------------------------------------------------'



'-----------------------------------------------------------------------------'
'Anzeigen der Daten'
'strSQL und einzelen Felder �nderbar'
'-----------------------------------------------------------------------------'
Sub PaintListe

'SQL Parameter f�r die Abfrage und Listenaufbau
strSQL="SELECT * from layout_mail;"

'Datenbankverbindung herstellen
Set objRS = Server.CreateObject("ADODB.Recordset")
objRS.Open strSQL,objConn, 1, 1

'Datemholen und Anzeigen
'Tabelle einleiten
call Paint_Tab_Start

if not objRS.EOF then
           Do until objRS.EOF
		       call Paint_Tab_Firstrow
			   call Paint_Tab_Action("?action=edit&detail=",objRS("id"),"edit","Edit")
			   call Paint_Tab_Row(objRS("ziel"))
			   call Paint_Tab_Row(objRS("quelle"))
               call Paint_Tab_Lastrow
               objRS.MoveNext
           Loop
	end if
    objRS.Close
call Paint_Tab_End
end sub
'-----------------------------------------------------------------------------'
'Anzeigen der Daten'
'-----------------------------------------------------------------------------'
%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<!--#include file="../../-Vscripts/logon.asp" -->
<!--#include file="../etc/edit_intro.asp" -->
<%

call Edit_Intro

Dim qryAction
Dim qryDetail
Dim qrySenden

qryAction=request("action")
qryDetail=request("detail")
qrySenden=request("senden")

if qrySenden = "Senden" then qryAction="Senden"
Select Case qryAction
        Case "edit"
		    CALL Edit(qryDetail)
 			CALL PaintListe
		Case else
            CALL PaintListe		
end select

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
</body>
</html>