<%
'21.12.12 - �berarbeitet Funktionen zusammengefasst und bereinigt.
Option Explicit

%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->
<%


Function gfarb(wert ) 
	Dim fr 
	fr = wert


'-------Gelbe
    If InStr(1, wert, "limette", vbTextCompare) <> 0 Then fr = "geld"
	If InStr(1, wert, "gelb", vbTextCompare) <> 0 Then fr = "gelb"
	If InStr(1, wert, "yellow", vbTextCompare) <> 0 Then fr = "gelb"
	If InStr(1, wert, "orange", vbTextCompare) <> 0 Then fr = "orange"

'-------Gr�ne
	If InStr(1, wert, "gruen", vbTextCompare) <> 0 Then fr = "gruen"
	If InStr(1, wert, "gr�n", vbTextCompare) <> 0 Then fr = "gruen"
	If InStr(1, wert, "green", vbTextCompare) <> 0 Then fr = "gruen"
	
'-------Weise
	If InStr(1, wert, "weiss", vbTextCompare) <> 0 Then fr = "weiss"
	If InStr(1, wert, "white", vbTextCompare) <> 0 Then fr = "weiss"
	If InStr(1, wert, "beige", vbTextCompare) <> 0 Then fr = "beige"

'------Rot
	If InStr(1, wert, "rot", vbTextCompare) <> 0 Then fr = "rot"
	If InStr(1, wert, "red", vbTextCompare) <> 0 Then fr = "rot"
	If InStr(1, wert, "mauve", vbTextCompare) <> 0 Then fr = "lila"
	If InStr(1, wert, "violett", vbTextCompare) <> 0 Then fr = "violett"

'-------Blau
	If InStr(1, wert, "blau", vbTextCompare) <> 0 Then fr = "blau"
	If InStr(1, wert, "blue", vbTextCompare) <> 0 Then fr = "blau"

'-------Silber
	If InStr(1, wert, "silber", vbTextCompare) <> 0 Then fr = "silber"
	If InStr(1, wert, "silver", vbTextCompare) <> 0 Then fr = "silber"
'-------Grau	
	If InStr(1, wert, "grau", vbTextCompare) <> 0 Then fr = "grau"
	If InStr(1, wert, "grey", vbTextCompare) <> 0 Then fr = "grau"
	If InStr(1, wert, "gray", vbTextCompare) <> 0 Then fr = "grau"

'-------Schwarz
	If InStr(1, wert, "schwarz", vbTextCompare) <> 0 Then fr = "schwarz"
	If InStr(1, wert, "black", vbTextCompare) <> 0 Then fr = "schwarz"

'-------so andere
	If InStr(1, wert, "braun", vbTextCompare) <> 0 Then fr = "braun"
	If InStr(1, wert, "gold", vbTextCompare) <> 0 Then fr = "gold"
    If InStr(1, wert, "brown", vbTextCompare) <> 0 Then fr = "braun"
	If InStr(1, wert, "beige", vbTextCompare) <> 0 Then fr = "braun"

	gfarb = fr
End Function

'Pr�ft anhand einer Ser_ID das vorhandensein in der Cars Tabelle.
function ifInDB( nummer)
    ifindb=false
    strSQL="SELECT cars.SER_ID from cars WHERE (cars.SER_ID="& nummer &");"
	Set objRS = Server.CreateObject("ADODB.Recordset")
  	objRS.Open strSQL, objConn,1,1
   	if objRS.EOF then ifindb=true
	objRS.Close
end function

'Durcharbeiten der Bilderarschive nach Bilder ohne DB eintrag. Fehlbilder werden gel�scht.
'Pfad = Pfad zum Bilderordner, Ext = ".jpg" ...., Log = 1 gibt Log infos aus, Index = 1, falls Bider mit Ser_id_1 usw vorhanden sind sonst name = Ser_id
Sub FileCrawler ( Pfad, Ext, Log, Index)

	Dim objFSO, objFolder, objItem, Help

	Set objFSO=Server.CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(Server.MapPath(Pfad))
	For Each objItem in objFolder.Files
		if instr(1,objitem.name,Ext) <> 0 then
			if Index = 1 then 
				Help = left(objitem.name,instr(1,objitem.name,"_",1)-1) 	
			else
				Help = left(objitem.name,5)
			end if 
			if ifindb(Help) then 
				if Log = 1 then response.write "Delete : " & Server.MapPath(Pfad & objitem.name) & ";" & Pfad &";" & now() & "<br />"
				objFSO.DeleteFile Server.MapPath(Pfad & objitem.name), true
		end if 
	   end if 
   next

   Set objItem = Nothing
   Set objFolder = Nothing
   Set objFSo = Nothing
		
end sub

'Daten aus GW-Data �bernehmen.
Sub Datenuebertrag (log)
		'Gw-Data verbinden
		Dim strComplete_New	, db_new
		strComplete_New = "PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=" & Server.MapPath("/-db/gw-data.mdb") & ";Jet OLEDB:Database Password=;"
		set db_new = Server.CreateObject("ADODB.Connection")
		db_new.open strComplete_New

		Dim RS_New
		Set RS_New = Server.CreateObject("ADODB.Recordset")
		Dim RS_V1
		Set RS_V1 = Server.CreateObject("ADODB.Recordset")
	
		Dim strSQL_New
		Dim strSQL_V1
		Dim theNewestDate
		'Der neuste Datensatz in der GW-BW'
		theNewestDate=now()-720
		Dim DateHelper

		'Schritt 1: �bertragen der Fahrzeugdaten 

		'Alle Fahrzeug im CMS erst l�schen
		strSQL_v1 = "DELETE * FROM cars;"
		RS_V1.Open strSQL_V1, objConn, 1, &H0002
		if log = 1 then response.write "Cars l�schen <br />"

		'�bertragung beginnen	
		strSQL_V1 = "Select * FROM cars;"
		RS_V1.Open strSQL_V1, objConn, 1, &H0002    
		strSQL_New = "Select * FROM holderbaum_web_v2;"
		RS_New.Open strSQL_New, db_new, 1, &H0002    
    
		Dim a
		Do While (Not RS_new.EOF)
		RS_V1.AddNew
		for a = 0 to 30
			RS_V1(a) = RS_New(a)
			if log = 1 then response.write "Cars f�llen : " & RS_New(a) & " : " & now() & "<br />"
		next
		RS_V1("grundfarbe")=gfarb(RS_New("lack"))
		RS_V1("emiss_co2_mix")=RS_New("emiss_co2_mix")
		RS_V1("consump_mix")=RS_New("consump_mix")
		RS_V1("consump_road")=RS_New("consump_road")
		RS_V1("consump_city")=RS_New("consump_city")
		RS_V1("allwheel")=RS_New("allwheel")
		RS_V1("motor_id")=RS_New("motor_id")
		RS_V1("DATE_CREATED")=RS_New("DATE_CREATED")
		RS_V1("hsn")=RS_New("hsn")
		RS_V1("tsn")=RS_New("tsn")
		RS_V1("garantie")=RS_New("garantie")
		RS_V1("care_status")=RS_New("care_status")
		RS_V1("was_commercial_use")=RS_New("was_commercial_use")
		RS_V1("euromobil")=RS_New("euromobil")
		RS_V1("emis_badge")=RS_New("emis_badge")
		RS_V1("emis_standard")=RS_New("emis_standard")
		RS_V1("chassisno")=RS_New("chassisno")
        RS_V1("reseller")=RS_New("reseller")
		RS_V1("owner_count")=RS_New("owner_count")
		RS_V1("CAR_SEATS")=RS_New("CAR_SEATS")
		RS_V1("hints")=RS_New("hints")
		if cint(RS_New("reseller")) >= cint(RS_New("preis")) then
			rs_v1("reseller_view")=1
			rs_v1("preis") = cint(RS_New("reseller")) + 1
		else
			rs_v1("reseller_view")=0
		end if
		
		
		DateHelper=RS_New("DATE_CREATED")
		'Der neuste Datensatz in der GW-BW'
		if theNewestDate <=DateHelper then theNewestDate =DateHelper 
    	RS_V1.Update
		if log = 1 then response.write "<br />"
		RS_New.MoveNext
    loop	
RS_V1.Close
RS_New.Close

end sub



Sub MenuGen(log)
	
	Dim del_Feld
		del_Feld="Fahrzeuge"

	Dim	i_Feld, strSQL_V1, RS_V1
    
		Set RS_V1 = Server.CreateObject("ADODB.Recordset")
	
		strSQL_V1 = "SELECT layout_navigation_head.name, layout_navigation_head.id FROM layout_navigation_head WHERE (((layout_navigation_head.name)='" & del_Feld & "'));"
		RS_V1.Open strSQL_V1, objConn, 1, &H0002 
		RS_V1.MoveFirst
			i_Feld=RS_V1("id")
		RS_V1.Close
		if log = 1 then response.write "Menu ID : " & i_Feld
		
		strSQL_V1 = "DELETE layout_navigation.*, layout_navigation_head.name, layout_navigation.ordnung FROM layout_navigation INNER JOIN layout_navigation_head ON layout_navigation.head = layout_navigation_head.id WHERE (((layout_navigation_head.name)='" & del_Feld & "') AND ((layout_navigation.ordnung)=100));"
		RS_V1.Open strSQL_V1, objConn, 1, &H0002 
   		strSQL_V1="INSERT INTO layout_navigation ( Link, ordnung, Titel, head ) SELECT 'default.asp?action=carlist&modelle=*&detail=' & [hersteller] AS link, 100 AS ordnung, cars.hersteller, " & i_Feld & " AS head FROM cars INNER JOIN cars_hersteller ON cars.hersteller = cars_hersteller.h_hersteller GROUP BY 'default.asp?action=carlist&modelle=*&detail=' & [hersteller], 100, cars.hersteller, " & i_Feld & ", cars_hersteller.h_order ORDER BY cars_hersteller.h_order desc;"
		RS_V1.Open strSQL_V1, objConn, 1, 1
		
end sub



call Datenuebertrag (1)
call MenuGen (1)
'Bilderl�schen!
Call FileCrawler ( "/-img/fzg/icon/", ".jpg", 1, 1)
Call FileCrawler ( "/-img/fzg/small/", ".jpg", 1, 1)
'Bilder dieser Ordnung werden zu Zeit nicht verwendet.
'Call FileCrawler ( "/-img/fzg/big/", ".jpg", 1, 1)
Call FileCrawler ( "/-img/fzg/qr/", ".png", 1, 0)
%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
   

