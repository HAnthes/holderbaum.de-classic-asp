<%
'Mobile CSV Export
'Mobile Schnittstellenbeschreibung vom 04.Mai.2006
'Treibstoffarten Liste EVA Stand 11.07.06
'Blickf�nger eingef�hrt. Datenbankangepasst - Tab:cars_info Feld mobile_blickfang
'23.11.06: Metalic per Instr auf metal und Perl ausgewertet
'23.11.06: nichtmehr grundfarbe sonder Lackbezeichung als Farbname
'28.11.06: Farbe wieder neu, aus Grundfarbe und Lack zusammengesetzt
'12.12.06: AJ: Lederr per Instring aus BE
'15.02.07 HSN und TSN  (DL,DK) nach Mobile Schnittstelle 30.01.07
'22.11.07 Garantie AE wird ber�cksichtig. All Garantietypen zu 1 sonst 0
'07.12.07 �nderung der Mobile schnittstelle, Erste Seite Buchung.
'18.02.08 Anpassung an die Schnittstellenbeschreibung vom 30.01.08
'	      Bild ID bei AA
'              alles in ""
'29.02.08: Autobild wegen wieder auf SER_id umgestellt !
'12.09.08: Schnittstellenbeschreibung vom 25.08.08 �bernommen. Einige Felder nicht umgesetzt.
'20.01.09: Mobile Update vom 12.01.09 2000 Zeichen Beschreibung (Z) Hubraum (BA)
'17.06.09: Filter f�r Ausstattungsmerkmale angepasst nach zubcollect liste.
'29.10.09: Mobileschnittesllenbeschreibung 01.10.09 eingearbeitet Punkte ER - EX m�ssen und eingearbeitet werden. Leerfelder werden geliefert!
'05.11.09: Navigationssystem �ber das Feld BE erkannt.
'10.12.09: Mobile Finanzierungsdaten, Schnittstellenbeschreibung 16.11.2009
'10.12.09: Vorbereitung und Felddefinition
'22.01.10: Wieoft()  auf 10 Leerstellen pr�fen lassen
'22.01.10: �nderungen der Mobile Schnittstelle �bernommen "Tagesangebot" hei�t jetzt "unsere Empfehlung"
'29.01.10: Finanzierungsdaten werden �bertrage (BETA) Pr�fung noch unsicher. Feld Finanzierungsfeature wird aus 102.asp gef�ttert
'01.02.10: MobMetaKomma zur Listenerzeugung (EVA-Export liefert <MetaKomma>
'08.03.10: �nderungen f�r Mobile Schnittstelle 17.Feb.2010
'28.03.10: Topinserat Mobile	
'23.06.10: Mobiel Schnittstelle vom  11.06.2010 
'29.10.10: Airbags Teill�sung eingebaut.
'17.12.10: Finanzierung offen und ohne Funktion. 
'17.12.10: FinTestErg=0 damit keine Finanzierungsdaten �bernommen werden. 
'27.12.10: Sitze (60) werden �bernommen. Sitzefeld wird leer gelassen wenn keine Angebaben.
'27.12.10 Airbag erkennung verkessert
'07.01.11  Gerne unterbreiten wir Ihnen ein individuelles Finanzierungs- oder Leasingangebot welches auch ohne Anzahlung bis 84 Monate m�glich ist!!! 
'12.05.11  Metalic (FELD AB) mit intsr auf metal pr�fen, Feld aus DB wird nicht mehr br�ckrichtig.
'20.12.12  Mobilebeschreibung 04.12.12 umgesetzt. Fehler bei Airbags und Auswertgung Leder nich 100%
'23.05.13  Mobilebeschreiung Features - 171 - 194 �ber Hints aus EVA �ber nommen - PDF an den Verkauf 
'29.01.14  UV - Topinserat eingef�gt
'20.01.14  Innenausstattung mit R�s abgesprochen und Auswertung eingebaut "Innenausstattung:farbe" bei der ausstattung, erlaubt sind schwarz, beige, grau, braun
'11.06.2014	 Neuwagen umsetzung in EVA �ber Hints {neu;A;DE}   neu;Co2Effizientzklasse;Landeskennung. in DB aufgeschl�sselt
			'Mobile Neuwagen <500km, kein ez, lieferfrist, Landesversion, Co2Effizenz 
			'Felder : 106, 21, 167,169
'06.10.2014 Mobile Emvk Elektro 206 und 207 festgelegt
Option Explicit

%>

<!--#include file="../../-Vscripts/v1_parameter.asp" -->
<!--#include file="../../-Vscripts/v1_db_connect.asp" -->

<%
'Funktionenn zur EVA Categorie selektion 

'Listet alles Kategoeren alle Bin array  FZ-Ident als Parameter
Dim EVACATARR(400,2)
Dim EVA_Max

function EVA_Para_DB_Init 
	Dim LobjRS
	Dim x
	strSQL="SELECT evo_translate.mobile_tag, evo_translate.Beschreibung, evo_translate.eva_id, evo_translate.mobile_Par FROM evo_translate;"
	Set LobjRS = Server.CreateObject("ADODB.Recordset")
	LobjRS.Open strSQL,objConn, 1, 1
	if not LobjRS.EOF then
		Do until LobjRS.EOF
				EVACATARR(x,1) = LobjRS("mobile_tag") 
				EVACATARR(x,0) = LobjRS("eva_id") 
				EVACATARR(x,2) = LobjRS("mobile_Par") 
				x=x+1
               LobjRS.MoveNext
         Loop
	end if 
	LobjRS.Close
	EVA_Para_DB_Init = x-1
end function


'Zeile - Parameter pr�fung
function EVA_Para(strCat,MobZeile)
Dim evoCatArray
dim i,j
Dim Ar_MobZeile
Dim Ar_eva_ID
Dim Ar_MobPar
Dim help,help1
'strCat splitten
evoCatArray = Split(strCat,";")
help1=0
if Ubound(evoCatArray) > 0  then 
	'Suchen der EVA-ID in Array
	for i=0 to EVA_MAX
		if EVACATARR(i,1) = MobZeile then
				Ar_eva_ID = EVACATARR(i,0)
				Ar_MobPar = EVACATARR(i,2)
				Ar_MobZeile = EVACATARR(i,1)
				for j = 0 to UBound(evoCatArray) - 1 
					if evoCatArray(j) = Ar_eva_ID then 
						help = Ar_MobPar
						if help > help1 then 
							help1=help
						end if 
						'response.write "- " & i & " - " & Ar_eva_ID & " - " & Ar_MobPar & " - " & Ar_mobZeile & "<br />"
					end if 	
				next
		end if   
	next
end if 
EVA_Para = help1
end function






'Ersetzt MetaKomma in Mobile Liste * = Einleitung \\ = Terminierung
'Mobile Line = ---- 
'Bold = ** 
Function MobMetaKomma(be)
	MobMetaKomma="* "&replace(be,"<MetaKomma>", "\\* ")&"\\"
end function

Function NormKomma(be)
	NormKomma=replace(be,"<MetaKomma>", ", ")
end function

function GetTex(bzsys)

	strSQL="SELECT * from cars_konvert_text where System='"& bzsys & "' order by rang;"
	Dim LobjRS
	
	Dim help
	'Datenbankverbindung herstellen
	Set LobjRS = Server.CreateObject("ADODB.Recordset")
	LobjRS.Open strSQL,objConn, 1, 1

          Do until LobjRS.EOF
		       help = help & LobjRS("text")
			    LobjRS.MoveNext
           Loop
		   GetTex = help
end function

function fueltest2(eva_id,parameter)
	strSQL="SELECT * from car_engine where eva_id=" & eva_id & ";"
	Dim LobjRS
	
	Dim strEVAText
	Dim strGruppe
	Dim mobile_df
	'Datenbankverbindung herstellen
	Set LobjRS = Server.CreateObject("ADODB.Recordset")
	LobjRS.Open strSQL,objConn, 1, 1
	strEVAText="nicht Vorhanden"
	strGruppe="nicht Vorhanden"
	mobile_df=0
	if not LobjRS.EOF then
          Do until LobjRS.EOF
		       strEVAText=LobjRS("motor")
			   strGruppe=LobjRS("gruppe")
			   mobile_df=LobjRS("mobile_df")
	           LobjRS.MoveNext
           Loop
	end if
	  LobjRS.Close
	Select case parameter
	case 1
	   fueltest2=strEVAText
	case 2
		fueltest2=strGruppe
	case 3
		fueltest2=mobile_df
	case 4
		if (eva_id=4) or (eva_id=51) or (eva_id=1) then fueltest2="Autogas"
	End Select	

	
end function

Function wieoft(wert1 , wert2 )
Dim x,y
	x = 1
	y=0
	x = InStr(x, wert1, wert2)

Do Until x = 0
    
    x = x+len(wert2)
    x = InStr(x, wert1, wert2)
    y=y+1
Loop
wieoft=y
End Function


Function checkit(modell)
'Funktion �berholt ... 28.07.2013
'if (instr(modell,"T�r") <> 0) or (wieoft(modell," ")>10) or (instr(modell,"gang") <> 0) or (instr(modell,"t�r") <> 0) or (instr(modell,"gang") <> 0) or ( len(modell)>48 ) then
 checkit=0
'end if

end function

'pr�fen ob findaten sinnvoll
Function finTest(anz,lauf,eff,mtl,rest)
'    Rest und Anzahlung k�nnen 0 sein.
'    Laufzeit kann nicht 0 sein
'    Zins kann nicht 0 sein
'    mtl kann nicht 0 sein. 
'   Laufzeit = 0 oder Leer keine Finanzierung.

    finTest=1
	'if anz=NULL then anz=0
	if lauf=0 then 
	'if mtl=NULL then mtl=0
	'if rest=NULL then rest=0
	'if anz=0 or lauf=0 or mtl=0 or rest=0 then 
	  finTest=0
	end if 
end function

Dim ExZeile
Dim help, varerror
Dim FinTestErg
'AS24 umschaltung : 20.12.122
Dim ExportModell
Dim MobZeile
Dim neuwagen
Dim benzin
ExportModell = request("EM")
'response.write ExportModell

'EVO Init DB
EVA_MAX =  EVA_Para_DB_Init()
	
    
	'strSQL="SELECT * from cars;"
	strSQL="SELECT cars.*, cars_info.mobile_blickfang,cars_info.ersteseite,cars_info.finfeature, cars_info.anz, cars_info.lauf, cars_info.eff, cars_info.mtl, cars_info.rest, cars_info.topinserat, cars_info.youtubelink FROM cars LEFT JOIN cars_info ON cars.SER_ID = cars_info.seri;"
	set objRS = Server.CreateObject("ADODB.Recordset")
	objRS.Open strSQL, objConn, 1, 1
	do while not objRs.Eof
    if checkit(objRS("modell"))=1 then 
	  varerror=varerror & "FZD ID:" &objRS("ser_id") &", Fahrzeug:"&objRS("hersteller") &" "& objRS("modell") &vbCrLf
	else
	
	
	'Check auf  Neuwagen nach Mobile!
	neuwagen = 0
	if objRS("neuwagen")=1 and objRS("TACHO") < 500 and objRS("co2eff")<>"" then
		   neuwagen =1
	end if
'-----------------0-10-A-K
'000	
	'A: Satznummer, Ganzzahl.  - 11.07.2013 
	help= """" & objRS("ser_id") & """"
	ExZeile = ExZeile & help &";"
	

'001	
	'B: Interne Nummer, Text, max 40 Zeichen. - 11.07.2013
	help= """" & objRS("ser_id") & """"
    ExZeile = ExZeile & help &";"
	
	
'002		
	'C: Kategorie, Text. Wird beim EVA-Exportscript erzeugt. 11.07.2013 - Beschreibung Mobile. 09.07.zusammensetzung nach Mobile Vorgaben Car.xxxxx
	'help="""" & objRS("Aufbau")  & """"
	'26.07.2013 : Fahrzugklasse nur Mobile Klasse "Car" ausgwertet, VEH_Typen direkt aus EVA, keine �bersetung durch Parametertabellen
	Select case cint(objRS("VEH_TYPE"))
		case 4
			help="Car.Cabrio"
		case 6,19
			help="Car.OffRoad"
		case 27
			help="Car.SmallCar"
		case 3
			help="Car.EstateCar"
		case 2
			help="Car.Limousine"
		case 5,26
			help="Car.SportsCar"				
		case 7
			help="Car.Van"				
		case else
			help="Car.OtherCar"	
	End Select
	help="""" & help & """"
	ExZeile = ExZeile & help &";"
	
	
'003		
	'D:Marke, Text. 11.03.2013
	help="""" & objRS("hersteller")  & """"
	ExZeile = ExZeile & help &";"

	
'004	
	'E:Modell, Text, 48 Zeichen. 11.07.2013
	help="""" & left(objRS("modell"),48)  & """"
	ExZeile = ExZeile & help &";"

	
'005		
	'F: Leistung in kW, Ganzzahl. 11.07.2013
	help= """" & objRS("leistung") & """"
	ExZeile = ExZeile & help &";"

	
'006		
	'G: HU, Text, Format mm.jjjj. 11.07.2013
	help=objRS("tuv")
	if help ="NEU" then help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'007		
	'H: AU, Text, Format mm.jjjj. 11.07.2013
	help=objRS("au")
	if help ="NEU" then help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'008		
	'I: EZ, Text, Format mm.jjjj Wenn Neufahrzeug leer. Neufahrzeuge sind mit NEU in der Modellbezeichung gekennzeichnet.  11.07.2013 (NEU Tag..???? wird nicht genutzt)
	help=objRS("ez")
	if neuwagen=1 then
	   help =""
    else
	   help=right(help,7)
	end if
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'009		
	'J:Kilometer, Ganzzahl. Wenn Neufahrzeug max 500km. 11.07.2013 - Neufahrzeugpr�fung wird nicht genutzt
	help= """" & objRS("tacho") & """"
	ExZeile = ExZeile & help &";"


'010		
	'K:Preis, Diff Steuer Brutto Regel Netto. 11.07.2013 - If verschachtelung aufgel��t - Pr�fen!!!
	'if objRS("gesch_art") = 0 then
		help = FormatNumber(objRS("preis"),2,0,0,0)
	'else
	'	help = objRS("preis")
    'end if		
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'-----------------0-10-A-K	

'----------------11-20-L-U		
'011		
	'L:MwSt: 0 = ausweisbar, 1 = nicht Ausweisbar. 11.07.2013
	help= """" & objRS("gesch_art") & """"
	ExZeile = ExZeile & help &";"

	
'012		
	'M: - Leerfeld. 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'013	
	'N: Oldtimer,  0=keiner, 1=Oldtimer. 11.07.2013, nie Oldtimer!
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'014	
	'O: Fahrgestellnummer (info). 11.07.2013
	help= """" & objRS("chassisno") & """"
	ExZeile = ExZeile & help &";"

	
'015	
	'P: Unfallfahrzeug. Nie Unfallfzg : 11.07.2013 - pr�fung ob aus EVA �bertragbar
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'016	
	'Q: 29.01.09 - wieder lack bezeichung nach R�s	/ EVA. 11.07.2013
	help= """" & objRS("lack") & " / " & objRS("grundfarbe") & """"
	if len(help) > 32 then help = """" & objRS("lack") & """"
	ExZeile = ExZeile & help &";"

	
'017	
	'R: Klima - EVA Category auswertung. 11.03.2013
	help = EVA_Para(objrs("category"),"17")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
		
		
'018		
	'S: Taxi. Nie Taxi. 11.07.2013
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
	
'019	
	'T: Behindertengerecht,  0=keiner, 1=Ja - nie. 11.07.2013 - Pr�fung ob EVA auswertbar.
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'020	
	'U:Jahreswagen, 0=nein,1=ja - Pr�fung nach Mobile Sematik. 11.07.2013
	help=0
	if  (DateDiff("m",objRS("ez"),Date())<=15) and (InStr(objRS("modell"),"NEU")=0)	then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------11-20-L-U		


'----------------21-30-V-AE			
'021	
	'V:Neuwagen, 0=nein,1=ja : 11.06.2014 - Nutzung
	help=0
	if  (neuwagen=1) then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'022	
	'W: "unsere empfehlung",  0=keiner, 1=Ja: nie - 11.07.2013 : pr�fung ob nutzbar in EVA
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'023	
	'X:H�ndlerpreis:Zahl - wird nur gesetzt wenn Preis auch tats�chlich kleiner als VK : 11.07.2013
	help=""
        if (objRS("reseller")>=1) and (objRS("reseller")<objRS("preis")) then help = objRS("reseller")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'024	
	'Y: reserviertes Feld immer leer : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'025 : Splitt in AS4 fomrat beim Irrtum Text
    'Variante Mobile
	'Z: Bemerkung Max 2000 Zeichen
	'11.07.2013 - eingek�rzt
	
	'In die B�rsenbearbeitung - wegen Gebrauchtwagen und Kommabehandlung. 
	'help=MobMetaKomma(left(replace(objRS("be"),"!",""),2000))
	'Ausklammer das 2x Irrt�mer.....
	if instr(objRS("be"),"Irrt�mer") then
		help= """" & MobMetaKomma(left(replace(objRS("be"),"!",""),2000)) & """"
	else
		Select case ExportModell
		Case "AS24"
			help="""" & MobMetaKomma(left(replace(objRS("be"),"!",""),2000)) & GetTex("AS24")
		Case "MOB"
			help="""" & MobMetaKomma(left(replace(objRS("be"),"!",""),2000)) & GetTex("Mobile")
		Case "GWB"
			help="""" & NormKomma(left(replace(objRS("be"),"!",""),2000)) & GetTex("Gebrauchtwagen")
		End Select
	end if
	if len(help) > 2000 then 
		varerror=varerror & "FZD ID:" &objRS("ser_id") & "Text zulang, wurde auf 2000 Zeichen gek�rzt" &vbCrLf
		help = left(help,2000)
	end if 
	ExZeile = ExZeile & help &""";"

	
'026	
	'AA:Bild ID (Ser_id) / Siehe Satznummer 0/A: 11.07.2013
	help="""" & objRS("ser_id")  & """"
	ExZeile = ExZeile & help &";"

	
'027	
	'AB: Auswertung �ber EVACat 27 : 11.07.2013
	help= EVA_Para(objRS("category"),"27")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'028	
	'AC:W�hrung, Text, 3 Zeichen nach ISO-4217 immer! - 11.07.2013
	help="EUR"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'029	
	'AD:MwSt Satz, v1_parameter hinterlegt - 11.07.2013
	if objRS("gesch_art") = 0 then
		help = VarMwSt
	else
		help = 0
    end if		
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'030	
	'AE: nie : 11.07.2013 - pr�fen ob EVA �bertragbar
    help=0
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------21-30-V-AE	


'----------------31-40-AF-AO	
	
'031	
	'AF:Alufelgen : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"31") & """"
	ExZeile = ExZeile & help &";"


'032	
	'AG: ESP : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"32") & """"
	ExZeile = ExZeile & help &";"

	
'033	
	'AH: ABS : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"33") & """"
	ExZeile = ExZeile & help &";"

	
'034	
	'AI: AHK, aus DB, extrafeld in EVA. 11.07.2013
	help= """" & objRS("ahk")*-1 & """"
	ExZeile = ExZeile & help &";"

	
'035 - 4.12.12, reserviert : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'036	
	'AK : �ber EVA_PARA 36 : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"36") & """"
	ExZeile = ExZeile & help &";"


'037	
	'Navi : �ber EVA_PARA 37 : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"37") & """"
	ExZeile = ExZeile & help &";"

	
'038	
	'AM : �ber EVA_PARA 38 : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"38") & """"
	ExZeile = ExZeile & help &";"

	
'039	
	'AN : �ber EVA_PARA 39 : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"39") & """"
	ExZeile = ExZeile & help &";"

	
'040	
	'AO : �ber EVA_PARA 40 : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"40") & """"
	ExZeile = ExZeile & help &";"
'----------------31-40-AF-AO	


'----------------41-50-AP-AY	
'041	
	'AP: Allrad 1=ja 0=nein : 0 Pr�fung wegen leerliefergun: 11.07.2013
	help=0
	if objRS("allwheel")=1 then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
	
'042 	
	'AQ: anzahl der T�ren : 11.07.2013
	help= """" & objRS("Tueren") & """"
	ExZeile = ExZeile & help &";"
	
	
'043	
	'AR:Umweltplakette : EVA <> Mobile umsetztung. 11.07.2013
	help=objRS("emis_badge")
	Select case help
		case 1
			help=1
		case 2
			help=4
		case 3
			help=3
		case 4
			help=2
		case else
			help=1	
	End Select
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'044	
	'AS:Servolenkung : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"44") & """"
	ExZeile = ExZeile & help &";"

	
'045	
	'AT:Biodieself�hig (12.09.08) ungel�st : 11.07.2013
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'046	
	'AU:Scheckheft, nur Motorr�der und KFZ : 11.07.2013
	if objRS("care_status") then
		help= "1"
	else
		help= "0"
	end if 
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'047	
	'AV:kat, nur Motorr�der
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'048	
	'AW:Kickstarter, nur Motorr�der
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'049	
	'AX:E-Starter, nur Motorr�der
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'050	
	'AY:Vorf�hrfahrzeug - haben wir nicht - 11.07.2013
	help=0
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------41-50-AF-AY


'----------------51-60-AZ-BI	
'051	
	'AZ: Antrieb, nur Motorr�der
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'052	
	'BA:alle Fahrzeuge : 11..03.2013
	help= """" & objRS("hubraum") & """"
	ExZeile = ExZeile & help &";"
	
'053	
	'BB: Tragkraft, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'054	
	'BC: Nutzlast, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'055
	'BD: Gesamtgewicht, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'056	
	'BE: Hubh�he, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'057	
	'BF: Bauh�he, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'058	
	'BG: Baujahr, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'059	
	'BH:Betriebsstunden, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'060	
	'BI: Sitze, nur Nutzfahrzeuge bis 7.5 Tonnen
	help=""
	if objRS("CAR_SEATS")<>"" then help=objRS("CAR_SEATS")
	help="""" & help & """"
	ExZeile = ExZeile & help &";"
'----------------51-60-AZ-BI	



'----------------61-70-BJ-BS	
'061	
	'BJ:Schadstoff : EVA-Mobileumsetzung : 11.07.2013
	help=objRS("emis_standard")
	
	Select case help
	case 1
	   help=1
	case 2
		help=2
	case 3
		help=3
	case 4
		help=3	
	case 5
		help=4
	case 6
		help=4
	case 7
		help=5
	case 8
		help=6
	End Select

	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'062	
	'BK:Kabinenart, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'063	
	'BL:Achsen, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'064	
	'BM:Tempomat  : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"64") & """"
	ExZeile = ExZeile & help &";"
	
'065
	'BN: Standheizung : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"65") & """"
	ExZeile = ExZeile & help &";"

'066	
	'BO:Kabine, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'067	
	'BP:Schutzdach, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'068	
	'BQ:Vollverkleidung, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'069	
	'BR:Komunal, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'070	
	'BS:Kran, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------61-70-BJ-BS



'----------------71-80-BT-CC	
'071	
	'BT:Retarder, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'072	
	'BU:Schlafplatz, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'073	
	'BV:TV : EVA_PAR 73  : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"73") & """"
	ExZeile = ExZeile & help &";"

	
'074	
	'BW:WC, nur Nutzfahrzeuge
	help= """" & EVA_Para(objRS("category"),"74") & """"
	ExZeile = ExZeile & help &";"
	
'075	
	'BX:Ladeboard, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'076	
	'BY:Hydraulik, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'077	
	'BZ:Schiebetuer, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'078
	'CA:Radtrommel, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'079	
	'CB:Trennwand, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'080	
	'CC:ebs: nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------71-80-BT-CC



'----------------81-90-CD-CM	
'081	
	'CD:vermietbar, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'082	
	'CE:Komperssor, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'083	
	'CF:Luftfederung, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'084	
	'CG:Scheibenbremsen, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'085	
	'CH:Fronthydraulik, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'086	
	'CI:BBS, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'087	
	'CJ:Schnellwechsel, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'088	
	'CK:ZSA, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'089	
	'CL:Kueche, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'090	
	'CM:K�hlbox, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------81-90-CD-CM	



'----------------91-100-CN-CW
'091	
	'CN:Schlafsitze, nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'092	
	'CO:Frontheber,  nur Nutzfahrzeuge
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'093	
	'CP:sichrbar nur f�r H�ndler aus cars - reseller_view : 11.07.2013
	help=objRS("reseller_view")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'094	
	'CQ:reserviert
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


	'095	
	'CR:ENVKV - Nur bei Neuwagen setzen? : 11.07.2013
	help=0
	if (cint(objRS("consump_mix"))<>0) and (cint(objRS("consump_road"))<>0) and (cint(objRS("consump_city"))<>0) and (cint(objRS("emiss_co2_mix"))<>0) then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'096	
	'CS:Verbaruch Innerorts : 11.07.2013
	help= """" & objRS("consump_city") & """"
	ExZeile = ExZeile & help &";"

	
'097	
	'CT:Verbrauch au�erorts : 11.07.2013
	help= """" & objRS("consump_road") & """"
	ExZeile = ExZeile & help &";"

	
'098	
	'CU:Verbauch mix : 11.07.2013
	help= """" & objRS("consump_mix") & """"
	ExZeile = ExZeile & help &";"


'099	
	'CV:Emissions mg : 11.07.2013
	help= """" & objRS("emiss_co2_mix") & """"
	ExZeile = ExZeile & help &";"


'100	
	'CW:Xenon : EVAPar : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"100") & """"
	ExZeile = ExZeile & help &";"
'----------------91-100-CN-CW

	
'----------------101-110-CX-DG
'101	
	'CX:Sitzheizung : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"101") & """"
	ExZeile = ExZeile & help &";"

	
'102	
	'CY:Partikelfilter : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"102") & """"
	ExZeile = ExZeile & help &";"

	
'103	
	'CZ:Einparkhilfe - f�llt nach 09.07.2013 weg
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'104	
	'DA:Schwackecode : hammer net : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'105	
	'DB:Lieferdatum : hammer net : 11.07.2013
	help=""
	if neuwagen = 1 then help= date()
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'106	
	'DC:Lieferfrist : von nmir festgelt f�r immer!
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'107	
	'DD:�berf�hrungskosten : hammer net : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'108	
	'DE: HU / AU neu :11.07.2013
	help=0
	if objRS("tuv") ="NEU" then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	

'109
    'DF:TReibstoffart : pfelge �ber Webseite: 11.07.2013
	help=fueltest2(objRS("motor_id"),3)
	benzin = help
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
	
'110	
	'DG:Getriebe : Umsetzung EVA-Mobile : 11.07.2013
	help=1
	if objRS("automatic")=-1 then help=3
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------101-110-CX-DG
	
	
'----------------111-120-DH-DQ	
'111	
	'DH:Exportfahrzeug
	help=0
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'112	
	'DI:Tageszulassung
	help=0
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'113	
	'DJ:Blickf�nger : Via Webseite : 11.07.2013
	help=objRS("mobile_blickfang")
	if help=1 then 
		help=1
	else
		help=0
	end if
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


	
'114 	
	'DK: HSN (Zahl) : 11.07.2013
    help= """" & objRS("hsn") & """"
	ExZeile = ExZeile & help &";"	

	
	
'115	
	'DL:TSN (Ziffern) : 11.07.2013
	help= """" & objRS("tsn") & """"
	ExZeile = ExZeile & help &";"

	
	
'116	
	'DM:Ersteseitebuchung : Via Webseite : 11.07.2013
	help=objRS("ersteseite")
	if help=1 then 
		help=1
	else
		help=0
	end if
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'117	
	'DN - reserviert : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'118	
	'DO - reserviert : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'119	
	'DP E10 fh�higkeit (12.09.02008 ohne l�sung) : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'120	
	'DQ Qualit�tssigel VW Firstclass ...hammer net : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------111-120-DH-DQ	

	
'----------------121-130-DR-EA		
'121	
	'DR  Pflanzenoel f�hig (12.09.02008 ohne l�sung) : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	

'122	
	'DS  Harnstoftank ????? : 11.07.2013 AdBlue schlabber
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'123	
	'DT Koffer motorrad
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'124	
	'DU Sturzb�gel Motorrad
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'125	
	'DV Scheibe Motorrad
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'126	
	'DW Standklima Nutz und Gro�fzg
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'127	
	'DX S-S Bereigung SMZ
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'128	
	'DY Strassenzulassung : BauMa
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'129	
	'DZ Etagenbett WoMo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"129") & """"
	ExZeile = ExZeile & help &";"
		
'130	
	'EA Festbett Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"130") & """"
	ExZeile = ExZeile & help &";"
'----------------121-130-DH-DQ	


'----------------131-140-EB-EK
'131	
	'EB : Heckgarage : Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"131") & """"
	ExZeile = ExZeile & help &";"

'132	
	'EC Markise : Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"132") & """"
	ExZeile = ExZeile & help &";"
	
'133	
	'ED Sep Dusche Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"133") & """"
	ExZeile = ExZeile & help &";"
	
'134	
	'EE Solar Anlage  Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"134") & """"
	ExZeile = ExZeile & help &";"

'135	
	'EF Mittelsitzgruppe Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"135") & """"
	ExZeile = ExZeile & help &";"

'136	
	'EG rundsitzgruppe  Womo   : 11.07.2013 
	help= """" & EVA_Para(objRS("category"),"136") & """"
	ExZeile = ExZeile & help &";"

'137	
	'EH Seitensizgruppe Womo   : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"137") & """"
	ExZeile = ExZeile & help &";"

'138	
	'EI : Hagelschaden nimmer    : 11.07.2013
	help="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'139	
	'EJ Schlafpl�tz  Womo   : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'140 L�nge Womo    : 11.07.2013
	'EK
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------131-140-EB-EK
	
	
'----------------141-154-EL-EY	
'141	
	'EL Breite Womo   : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'142	
	'EM H�he Womo   : 11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'143	
	'EN Laderaum LKW :11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'144	
	'EO Laderaum LKW :11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'145
	'EP Laderaum LKW :11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'146	
	'EQ Laderaum LKW :11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'147	
	'ER Laderaum LKW :11.07.2013
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'148	
	'Mobile Schnittstelle - 01.10.2009
	help = 0
	help= """" & help & """"
	ExZeile = ExZeile & help &";"


'Finanzierung nicht bearbeiten!
	'Pr�fung noch mehr als l�ckenhaft!
	'FinTestErg=finTest(objRS("anz"),objRS("lauf"),objRS("eff"),objRS("mtl"),objRS("rest"))
	FinTestErg=0

'149	
	'ET - effktiver Jahreszins - noch keine umsetzung durch mobile.asp
	help=""
	if FinTestErg=1 then
		help=objRS("eff")	
	end if	
	help= """" & help & """"
    ExZeile = ExZeile & help &";"

'150	
	'EU - monatliche Rate 
	help=""
	if FinTestErg=1 then
		help=objRS("mtl")	
	end if 
	help= """" & help & """"
    ExZeile = ExZeile & help &";"
	
'151	
	'Ev- Laufzeit
	help=""
	if FinTestErg=1 then
		help=objRS("lauf")	
	end if 
	help= """" & help & """"
    ExZeile = ExZeile & help &";"
	
'152	
	'EW - Anzahlung
	help=""
	if FinTestErg=1 then
		help=objRS("anz")	
	end if 
	help= """" & help & """"
    ExZeile = ExZeile & help &";"

'153	
	'EX - Schlussrate
	help=""
	if FinTestErg=1 then
		help=objRS("rest")	
	end if 
	help= """" & help & """"
    ExZeile = ExZeile & help &";"

'154	
	'EY - Finanzierungsfeature  (Finanzierung als Premium anzeigen!), 10Cent pro Tag 
	'Soll bei Finanzierungen Automatisch genbucht werden! 
	help=0
	if FinTestErg=1 and objRS("finfeature")=1 then
		help=1
	end if 
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------141-154-EL-EY


'----------------155-170-EZ-FO
'155
	'EZ: Interieurfarbe : nicht Auswertbar : 
	help = 5
	if instr(ucase(objRS("be")),ucase("innenausstattung:schwarz")) then help=1
	if instr(ucase(objRS("be")),ucase("innenausstattung:grau")) then help=2
	if instr(ucase(objRS("be")),ucase("innenausstattung:beige")) then help=3
	if instr(ucase(objRS("be")),ucase("innenausstattung:braun")) then help=4
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'156 
	'FA Interieurtype - Cat 156 Teilweise. 11.07.2013
	help= """" & EVA_Para(objrs("category"),"156") & """"
	ExZeile = ExZeile & help &";"

	
'157
	'FB: Airbag
	help= """" & EVA_Para(objrs("category"),"157") & """"
	ExZeile = ExZeile & help &";"

	
'158
	'FC: Vorbesitzer							11.07.2013
	'IN DB aus EVA Verarbeitet "owner_count" 
	'1=einer, 2=zwei, 3=drei, 4=vier, 5=mehr als vier.
	help=objRS("owner_count")	
	if help > 4 then help=5
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

	
'159
	' FD : Topinserat  3 Euro pro Tag, �ber Websweite buchbar - 11.07.2013
	help=0
	if objRS("topinserat")=1 then
		help=1
	end if 
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
	
'�nderungen 11.06.2010 - Schnittstellenbeschreibung Mobile.
'Finanzierung Daten noch ungel�st. Wird komplett stillgelegt.
'Berechnung des Modells nur f�r den Verkauf m�glich..

'160 FE Bruttokreditbetrag (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'161 FF Abschlussgeb�hren (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'162 FG Ratenabsicherung (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'163 FH Nettokreditbetrag (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'164 FI Anbieterbank (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'165 FJ Soll Zinssatz (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'166 FK Art des Zinssatzes (149 - 154)
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'167 FL Landesversion (Mob schnittstelle  24.08.2010)
	help=""
	if neuwagen = 1 then help=objRS("landeskennung")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'168 
	'FMVideo URL (Mob schnittstelle 04.03.2011)
	'Immer leer geliefer.  ..... 31.05.2011 - nicht mehr!
	help=""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'169 
	'FN Energieeffizienzklasse  : 20.12.12, nur f�r NW wichtig
	help=""
	if neuwagen = 1 then help = objRS("co2eff")
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'170
	'FO envkv_Benzin_sorte : 20.12.12, wird in EVA nicht gef�hrt
	help=""
	if benzin = 1 then help = "SUPER" 
	
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	benzin=99
'----------------155-170-EZ-FO

'----------------171-180-FP-FY
'Featurefelder in Mobile CSV per Category aus EVA - Beschreibung 8.5.2013	
'171
'FP el. Seitenspiegel : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"171") & """"
	ExZeile = ExZeile & help &";"	
	

'172
'FQ Sportfahrwerk : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"172") & """"
	ExZeile = ExZeile & help &";"		
	

'173
'FR Sportpackt : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"173") & """"
	ExZeile = ExZeile & help &";"		
	

'174
'FS Bluetooth : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"174") & """"
	ExZeile = ExZeile & help &";"		
	

'175
'FT Bordcomputer : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"175") & """"
	ExZeile = ExZeile & help &";"		
	

'176
'FU CD-Spieler : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"176") & """"
	ExZeile = ExZeile & help &";"		
	

'177
'FV el.Sitz : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"177") & """"
	ExZeile = ExZeile & help &";"		
	

'178
'FW Headup : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"178") & """"
	ExZeile = ExZeile & help &";"		
	

'179
'FX Freisprecheinrichtung : 11.07.2013
	help="0"
	if instr(objRS("be"),"freisprech") then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"

'180
'FY Mp3 Schnittstelle : 11.07.2013
	help="0"
	if instr(objRS("be"),"MP3") then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
'----------------171-180-FP-FY


'----------------181-190-FZ-GI
'181
'FZ Multifunktionslenkrad : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"181") & """"
	ExZeile = ExZeile & help &";"		
	
'182
'GA Skisack : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"182") & """"
	ExZeile = ExZeile & help &";"		

'183
'GB Tuner/Radio : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"183") & """"
	ExZeile = ExZeile & help &";"		

'184
'GC Sportsitze : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"184") & """"
	ExZeile = ExZeile & help &";"		

'185
'GD Panoramadach : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"185") & """"
	ExZeile = ExZeile & help &";"		
	
'186
'GE Kindersitzbef. : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"186") & """"
	ExZeile = ExZeile & help &";"		
	
'187
'GF Kurvenlicht : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"187") & """"
	ExZeile = ExZeile & help &";"		
	
'188
'GG Lichtsensor : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"188") & """"
	ExZeile = ExZeile & help &";"		

'189
'GH Nebelscheinwefer : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"189") & """"
	ExZeile = ExZeile & help &";"		

'190
'GI Tagfahrlicht : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"190") & """"
	ExZeile = ExZeile & help &";"		

'----------------181-190-FZ-GI

'----------------191-202-GJ-GU
'191
'GJ Tranktionskontrolle : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"191") & """"
	ExZeile = ExZeile & help &";"			
	
'192
'GK Start Stop : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"192") & """"
	ExZeile = ExZeile & help &";"			
	
'193
'GL regenseonsor : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"193") & """"
	ExZeile = ExZeile & help &";"			
	
'194 
'GM Nichtraucherfahrzeug
	help="0"
	if instr(objRS("be"),"nichtraucher") then help=1
	help= """" & help & """"
	ExZeile = ExZeile & help &";"
	
'195
'GN Dachreling : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"195") & """"
	ExZeile = ExZeile & help &";"

'196
'GO Unfallfrei : 11.07.2013
	help ="0"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"	
	
'197
'GP Fahrtauglich : 11.07.2013
	help ="1"
	help= """" & help & """"
	ExZeile = ExZeile & help &";"	

'198
'GQ Produktionsdatum : 11.07.2013
	help =""
	help= """" & help & """"
	ExZeile = ExZeile & help &";"	

'199
'GR Einparkhilfe vorn : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"199") & """"
	ExZeile = ExZeile & help &";"

'200
'GS Einpackhilfe hinten : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"200") & """"
	ExZeile = ExZeile & help &";"	
	
'201
'GT R�ckfahrklamera : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"201") & """"
	ExZeile = ExZeile & help &";"	

'202
'GU Einparklenk... : 11.07.2013
	help= """" & EVA_Para(objRS("category"),"202") & """"
	ExZeile = ExZeile & help &";"		
'203
'GV Topinserrat... : 29.01.2014  - bedeutung nicht gekl�rt Zeile 159 ebenfall Topinserat
	help= """" & "0" & """"
	ExZeile = ExZeile & help &";"	
'204
'GW Rostiftpreis ohne auswertung, 07.04.2014
	help= """" & "0" & """"
	ExZeile = ExZeile & help &";"	
'205
'GX ebayk export = ungekl�rt, keine beschreibung vorhanden, 07.04.2014
	help= """" & "0" & """"
	ExZeile = ExZeile & help &";"	

'206 
'GY Plugin Hybrid
	help= """" & "0" & """"
	ExZeile = ExZeile & help &";"	
	
'207 
'GY kombinierter Stromverbrauch
	help= """" & "" & """"
	ExZeile = ExZeile & help &";"	
	
'Schrebien der Zeile .....	
	ExZeile = ExZeile & vbCrLf 
	response.write ExZeile
	ExZeile = ""
end if 
	objRs.MoveNext
	loop  	

if len(varerror)<>0 then 
	
	 Dim Mailer
		Set Mailer = Server.CreateObject("SMTPsvg.Mailer")
		Mailer.CharSet = 2
		Mailer.FromName = "www.holderbaum.de - Mobile Export"
		Mailer.FromAddress = "webvkmail@holderbaum.de"
		Mailer.RemoteHost = V1_MailHost
		Mailer.ClearAllRecipients
		Mailer.AddRecipient "WebVKMail", "webvkmail@holderbaum.de"
		Mailer.Subject = "Exportfehler - Mobile"
		Mailer.ClearBodyText
		Mailer.BodyText = "Das Fahrzeugexport-Modul hat f�r die Mobile DB folgenden Fahrzeuge als fehlerhaft verworfen."& vbCrLf & vbCrLf &varerror
	   Mailer.SendMail 
     Set Mailer = Nothing
 
end if 
	

%>
<!--#include file="../../-Vscripts/v1_db_close.asp" -->
   
